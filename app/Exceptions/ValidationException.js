'use strict'

const GE = require('@adonisjs/generic-exceptions')

class ValidationException extends GE.LogicalException {
  /**
   * Handle this exception by itself
   */
  handle (error, { response }) {
    response.status(error.status).json({
      message: error.message,
      status: error.status,
      code: error.code ? error.code : null,
      name: error.name ? error.name : null
    })
  }
}

module.exports = ValidationException

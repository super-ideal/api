'use strict'

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle (error, { response }) {
    switch (error.code) {
      case 'ECONNREFUSED': error.message = 'Neste momento estamos com problemas. Tente novamente mais tarde!'; break
      //case 'ER_BAD_FIELD_ERROR': error.message = 'Neste momento estamos com problemas. Tente novamente mais tarde!'; break
      // case 'ER_NO_SUCH_TABLE': error.message = 'Requisição não concluída. Tente novamente!'; break
      case 'ER_DUP_ENTRY': error.message = 'Os dados informados estão duplicados'; break
      case 'ER_NO_REFERENCED_ROW_2': error.message = 'Alguns dados informados não existe na tabela'; break
      case 'E_MISSING_DATABASE_ROW': error.message = 'Não foi encontrado nenhum resultado com os dados informados'; break
      case 'E_JWT_TOKEN_EXPIRED': error.message = 'O token expirou. Faça login novamente para continuar'; break
      case 'E_INVALID_JWT_TOKEN': error.message = 'O token não foi informado ou não é válido'; break
      case 'E_INVALID_JWT_REFRESH_TOKEN': error.message = 'O refresh token não é válido'; break
    }

    switch (error.name) {
      case 'ValidationException': error.status = 422; break
    }

    if (error.status === 429) {
      return response.status(error.status).json({ message: 'Há algo de errado com a sua solicitação. Tente novamente mais tarde!', status: error.status })
    }

    response.status(error.status).json({
      message: error.message,
      status: error.status,
      code: error.code ? error.code : null,
      name: error.name ? error.name : null
    })
    return
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report (error, { request }) {
  }
}

module.exports = ExceptionHandler

'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const Action = use('App/Models/Acl/Action')

class ActionService {

  /**
   * Cadastra uma nova ação
   * @param {*} data Objeto com os dados da ação
   */
  async store(data) {
    try {
      const result = await Action.create(data)
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca todas as ações cadastradas
   */
  async index() {
    try {
      const result = await Action.all()
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Exclui uma ação
   * @param {*} id Id único da ação
   */
  async deleteAction(id) {
    try {
      const result = await Action.findOrFail(id)
      await result.delete()

      return new Response('Ação EXCLUÍDA com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new ActionService()

'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const Page = use('App/Models/Acl/Page')

class AclService {

  /**
   * Cadastra uma nova página
   * @param {*} data Objeto com os dados da página
   */
  async store(data) {
    try {
      const result = await Page.create(data)
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca todas as páginas cadastradas
   */
  async index() {
    try {
      const result = await Page.query().orderBy('level', 'desc').fetch()
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Exclui uma Página
   * @param {*} id Id único da Página
   */
  async delete(id) {
    try {
      const result = await Page.findOrFail(id)
      await result.delete()

      return new Response('Página EXCLUÍDA com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new AclService()

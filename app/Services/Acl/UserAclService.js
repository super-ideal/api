'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')
const Database = use('Database')

const PageActionUserView = use('App/Models/Acl/PageActionUserView')
const PageActionUser = use('App/Models/Acl/PageActionUser')

class UserAclService {
  /**
   * Busca todas as permissões de um usuário
   * @param {*} idUser Id do usuário para buscar as permissões
   */
  async showPermissionsUserId(idUser) {
    try {
      const page = await PageActionUserView.query().where('user_id', idUser).fetch()
      return new Response(null, 200, page)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca todas as permissões do usuário que requisitou
   * @param {*} auth Dados de autenticação do usuário
   */
  async showPermissionsUser(auth) {
    try {
      const user = await auth.getUser()
      const permissions = await PageActionUserView.query().where('user_id', user.id).fetch()
      return new Response(null, 200, permissions)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Atualiza as permissões do usuário removendo ou adicionando permissões
   * @param {*} data Permissões a serem adicionadas ou deletadas
   */
  async update(data, idUser) {
    try {
      const trx = await Database.beginTransaction()

      // Exclui uma ou mais permissões de um usuário
      if (data['deletePermissions']) {
        await PageActionUser
          .query()
          .transacting(trx)
          .where({
            'user_id': idUser
          })
          .where('page_action_id', 'in', data['deletePermissions'])
          .delete()
      }

      // Cadastra as permissões do usuário
      let newPermissions = data['newPermissions']
      if (newPermissions) {
        data = []
        for (let i = 0; i < newPermissions.length; i++) {
          data.push({
            'user_id': idUser,
            'page_action_id': newPermissions[i]
          })
        }

        await PageActionUser.createMany(data, trx)
      }

      trx.commit()
      return new Response("Permissões do usuário cadastradas com sucesso!", 200, null)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new UserAclService()

'use strict'
const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const Partner = use('App/Models/Partner/Partner')
const PartnerPhone = use('App/Models/Partner/PartnerPhone')

class PartnerPhoneService {
  /**
   * Busca todos os telefones
   * @param {*} params Parâmetros para busca dos dados
   */
  async index(params) {
    try {
      const wBy = params.wBy ? params.wBy : 'number'
      const w = params.w ? params.w : ''
      const pagination = params.pagination = params.pagination ? JSON.parse(params.pagination) : false
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let query = PartnerPhone.query().whereRaw(wBy + ' like ?', ['%' + w + '%'])
      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.toJSON().length === 0)
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca os detalhes de um telefone
   * @param {*} id ID da tabela que contem os telefones
   */
  async show(id) {
    try {
      const result = await PartnerPhone
        .query()
        .whereRaw('id = ?', [id])
        .with('added', (builder) => {
          builder.select('id', 'username', 'partner_id')
        })
        .with('updated', (builder) => {
          builder.select('id', 'username', 'partner_id')
        })
        .first()

      if (result === null) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Salva os dados do telefone
   * @param {*} data Objeto com os dados que serão salvos
   * @param {*} idPartner ID do parceiro relacionado ao telefone
   */
  async store(data, idPartner) {
    try {
      const partner = await Partner.find(idPartner)
      const result = await partner.phones().createMany(data)
      return new Response('Telefone(s) CADASTRADO com sucesso!', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Atualiza os dados do telefone
   * @param {*} data Objeto com os dados que serão atualizados
   * @param {*} id ID da tabela que contem os telefones
   * @param {*} auth Dados de autenticação do usuário
   */
  async update(data, id, auth) {
    try {
      const user = await auth.getUser()
      const result = await PartnerPhone.findOrFail(id)

      // Adiciona o ID do usuário que está atualizando
      data['updated_by'] = user.id

      // Faz o merge dos dados e salva no BD
      result.merge(data)
      await result.save()

      return new Response("Telefone ATUALIZADO com sucesso!", 200, result)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Exclui um telefone
   * @param {*} id ID da tabela que contem os telefones
   */
  async delete(id) {
    try {
      const result = await PartnerPhone.findOrFail(id)
      await result.delete()

      return new Response('Telefone EXCLUÍDO com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new PartnerPhoneService()

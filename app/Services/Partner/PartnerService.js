'use strict'
const Response = use('App/Class/ResponseClass')
const ErrorException = use('App/Exceptions/ErrorException')
const Database = use('Database')

const Partner = use('App/Models/Partner/Partner')
const PartnerType = use('App/Models/Partner/PartnerType')

class PartnerService {
  /**
   * Busca todos os parceiros
   * @param {*} params Parâmetros para busca dos dados
   */
  async index (params) {
    try {
      const column = params.column ? params.column : 'name_reason'
      const operator = params.operator ? params.operator : 'anywhere'
      const text = params.text ? params.text : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const gBy = params.gBy ? params.gBy : null
      const user = params.user ? params.user : false
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let query = Database.table('partners as p')
      query.select('p.*')

      if (JSON.parse(user)) {
        query.leftJoin('users as u', 'u.partner_id', 'p.id')
        query.select('u.id as user_id')
      }

      if (operator == 'anywhere')
        query = query.whereRaw(column + ' like ?', ['%'+text+'%'])
      else if (operator == 'started')
        query = query.whereRaw(column + ' like ?', [text+'%'])
      else if (operator == 'finished')
        query = query.whereRaw(column + ' like ?', ['%'+text])
      else if (operator == 'equal')
        query = query.whereRaw(column + ' = ?', [text])
      else if (operator == 'different')
        query = query.whereRaw(column + ' != ?', [text])
      else if (operator == 'smaller')
        query = query.whereRaw(column + ' < ?', [text])
      else if (operator == 'bigger')
        query = query.whereRaw(column + ' > ?', [text])
      else if (operator == 'contained')
        query = query.whereIn(column, text.split(','))

      if (gBy.toString() != 'null' && gBy.toString() != 'undefined') {
        query = query.groupBy(params.gBy)
      }

      const result = await query.orderBy('p.' + oBy, oSort).paginate(page, perPage);

      if (result.data.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)
    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca os detalhes de um parceiro
   * @param {*} id ID da tabela que contem os parceiros
   */
  async show (id) {
    try {
      const partner = await Partner
        .query()
        .whereRaw('id = ?', [id])
        .with('partnerType')
        .with('adresses')
        .with('emails')
        .with('phones')
        .with('added', (builder) => {
          builder.select('id', 'username', 'partner_id')
          .with('partner', (builder) => {
            builder.select('id', 'name_reason')
          })
        })
        .with('updated', (builder) => {
          builder.select('id', 'username', 'partner_id')
          .with('partner', (builder) => {
            builder.select('id', 'name_reason')
          })
        })
        .with('user')
        .first()

      if (!partner) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, partner)

    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store (data, auth) {
    try {
      const user = await auth.getUser()
      data['partner']['added_by'] = user.id

      const partner = await Partner.create(data['partner'])

      if (data.typesPartner != undefined)
        await partner.partnerType().attach(data.typesPartner)

      if (data.emails != undefined)
        await partner.emails().createMany(data.emails)

      if (data.phones != undefined)
        await partner.phones().createMany(data.phones)

      if (data.adresses != undefined)
        await partner.adresses().createMany(data.adresses)

      return new Response('Dados cadastrados com sucesso!', 201, partner)

    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update (data, id, auth) {
    try {
      const user = await auth.getUser()
      const partner = await Partner.findOrFail(id)

      if (data.partner != undefined) {
        data['partner']['updated_by'] = user.id
        partner.merge(data.partner)
        await partner.save()
      }

      if (data.typesPartner != undefined)
        await partner.partnerType().sync(data.typesPartner) // recebe um array com os ids dos tipo de parceiro

      return new Response('Dados atualizados com sucesso', 200, partner)
    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca todos os tipos de parceiros
   */
  async typesPartners() {
    try {
      const result = await PartnerType.all()
      return new Response(null, 200, result)

    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new PartnerService()

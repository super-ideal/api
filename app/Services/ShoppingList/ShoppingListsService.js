'use strict'
const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const ShoppingLists = use('App/Models/Public/ShoppingLists/ShoppingListsView')

class ShoppingListsService {
  async index(params) {
    try {
      const column = params.column ? params.column : 'name'
      const operator = params.operator ? params.operator : 'anywhere'
      const text = params.text ? params.text : ''

      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const pagination = params.pagination = params.pagination ? JSON.parse(params.pagination) : true
      params.listProducts = params.listProducts ? JSON.parse(params.listProducts) : true

      let query = ShoppingLists.query()

      if (operator == 'anywhere')
        query = query.whereRaw(column + ' like ?', ['%'+text+'%'])
      else if (operator == 'started')
        query = query.whereRaw(column + ' like ?', [text+'%'])
      else if (operator == 'finished')
        query = query.whereRaw(column + ' like ?', ['%'+text])
      else if (operator == 'equal')
        query = query.whereRaw(column + ' = ?', [text])
      else if (operator == 'different')
        query = query.whereRaw(column + ' != ?', [text])
      else if (operator == 'smaller')
        query = query.whereRaw(column + ' < ?', [text])
      else if (operator == 'bigger')
        query = query.whereRaw(column + ' > ?', [text])
      else if (operator == 'contained')
        query = query.whereIn(column, text.split(','))

      if (params.listProducts) {
        query = query.with('listProducts', (builder) => {
          builder.with('product', (builder) => {
            builder.select('id', 'barcode', 'description')
              .with('unity')
              .with('images', (builder) => {
                builder.select('product_id', 'id', 'filename')
              })
          })
        })
      }

      query = query.orderBy(oBy, oSort)

      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.rows.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca os detalhes de uma lista de compra
   * @param {*} id ID da lista de compra
   */
  async show(idList) {
    try {
      const list = await ShoppingLists
        .query()
        .whereRaw('id = ?', [idList])
        .with('listProducts', (builder) => {
          builder.with('product', (builder) => {
            builder.with('images')
          })
        })
        .first()

      if (!list) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, list)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new ShoppingListsService()

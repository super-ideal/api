'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const AttributesTypeStyle = use('App/Models/Product/Attributes/AttributesTypeStyle')

class AttributesTypeStyleService {
  async index() {
    try {
      const result = await AttributesTypeStyle.query().fetch()
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new AttributesTypeStyleService()

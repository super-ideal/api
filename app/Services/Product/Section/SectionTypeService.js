'use strict'
const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const SectionType = use('App/Models/Product/Section/SectionType')

class SectionTypeService {
  async index(params) {
    try {
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const pagination = params.pagination = params.pagination ? JSON.parse(params.pagination) : false
      params.detailsSections = params.detailsSections ? JSON.parse(params.detailsSections) : false

      let query = SectionType.query()
      .with('parent', (builder) => {
        builder.select('id', 'name', 'id_unique')
      })

      if (params.detailsSections) {
        query = query.with('section', (builder) => {
          builder.where('active', true)
          builder.select('id', 'name', 'active', 'type_id')
          builder.orderBy('name', 'asc')
        })
      }

      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.length === 0)
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async show(id, params) {
    try {
      params.detailsSections = params.detailsSections ? JSON.parse(params.detailsSections) : false

      let query = SectionType.query().where('id', id)

      if (params.detailsSections) {
        query = query.with('section', (builder) => {
          builder.where('active', true)
          builder.select('id', 'name', 'active', 'type_id')
        })
      }

      const result = await query.first()
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(data) {
    try {
      const result = await SectionType.create(data)
      return new Response('Tipo de seção CADASTRADA com sucesso!', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(data, id) {
    try {
      const result = await SectionType.findOrFail(id)
      result.merge(data)
      await result.save()

      return new Response("Tipo de seção ATUALIZADA com sucesso!", 200, result)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async delete(id) {
    try {
      const result = await SectionType.findOrFail(id)
      await result.delete()

      return new Response('Tipo de seção EXCLUÍDA com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new SectionTypeService()

'use strict'
const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const SectionType = use('App/Models/Product/Section/SectionType')

class SectionTypePublicService {
  async show(idTypeSection, params) {
    try {
      params.detailsSections = params.detailsSections ? JSON.parse(params.detailsSections) : false
      const wBy = params.wBy ? params.wBy : 'id'
      const w = params.w ? params.w : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let result;
      const typeSection = await SectionType.findOrFail(idTypeSection)

      if (params.detailsSections) {
        result = await typeSection
          .section()
          .withCount('products', (builder) => {
            builder.where('active', 1)
          })
          .whereRaw(wBy + ' like ? ', ['%' + w + '%'])
          .orderBy(oBy, oSort)
          .paginate(page, perPage)
      } else {
        result = typeSection;
      }

      if (result.rows.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new SectionTypePublicService()

'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')
const Database = use('Database')

const Section = use('App/Models/Product/Section/Section')

class SectionService {
  async index(params) {
    try {
      const wBy = params.wBy ? params.wBy : 'name'
      const w = params.w ? params.w : ''
      const oBy = params.oBy ? params.oBy : 'name'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let active = params.active ? params.active : null
      if (active == 'true') { active = 1 } else if (active == 'false') { active = 0 }

      const result = await Database
        .table('product_section_type_view')
        .whereRaw(wBy + ' like ?', ['%'+w+'%'])
        .where(function () {
          if (active == 1 || active == 0) this.where('active', active)
        })
        .orderBy(oBy, oSort)
        .paginate(page, perPage)

      if (result.data.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      /*const result = await Section
        .query()
        .with('type', (builder) => {
          builder.select('id', 'name')
        })
        .whereRaw(wBy + ' like ? ', ['%'+w+'%'])
        .orderBy(oBy, oSort)
        .paginate(page, perPage)*/

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async searchSectionByTypes(idType) {
    try {
      const result = await Section
      .query()
      .select('id', 'active', 'name', 'description', 'type_id')
      .whereRaw('type_id = ? and active = 1', [idType])
      .fetch()

      if (result === null) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async show(id) {
    try {
      const result = await Section
      .query()
      .whereRaw('id = ? ', [id])
      .with('type', (builder) => {
        builder.select('id', 'name')
      })
      .first()

      if (result === null) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(data) {
    try {
      const result = await Section.create(data)
      return new Response('Seção CADASTRADA com sucesso!', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(data, id) {
    try {
      const result = await Section.findOrFail(id)
      result.merge(data)
      await result.save()

      return new Response('Seção ATUALIZADA com sucesso!', 200, result)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async delete(id) {
    try {
      const result = await Section.findOrFail(id)
      await result.delete()

      return new Response('Seção EXCLUÍDA com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new SectionService()

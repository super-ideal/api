'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const Product = use('App/Models/Product/Product')
const Section = use('App/Models/Product/Section/Section')

class ProductService {
  async index(params) {
    try {
      const wBy = params.wBy ? params.wBy : 'id'
      const w = params.w ? params.w : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let active = params.active ? params.active : null
      if (active == 'true') { active = 1 } else if (active == 'false') { active = 0 }

      let query = Product
        .query()
        .where(function () {
          if (active == 1 || active == 0) this.where('active', active)
        })
        .with('auxiliaryCode')
        .with('attributesProduct', (builder) => {
          builder.select('id', 'name', 'type_id')
          .with('type', (builder) => {
            builder.select('id', 'id_unique', 'public', 'style_id', 'required', 'name')
            .with('style')
          })
        })
        .with('sectionProduct', (builder) => {
          builder.select('id', 'name', 'type_id')
          .with('type', (builder) => {
            builder.select('id', 'id_unique', 'required', 'name')
          })
        })
        .with('created', (builder) => {
          builder.select('id', 'username', 'partner_id')
          .with('partner', (builder) => {
            builder.select('id', 'name_reason')
          })
        })
        .with('updated', (builder) => {
          builder.select('id', 'username', 'partner_id')
          .with('partner', (builder) => {
            builder.select('id', 'name_reason')
          })
        })
        .with('unity')

      if (wBy && w) {
        query = query.whereRaw('products.' + wBy + ' like ?', ['%' + w + '%'])
      }

      const result = await query.orderBy(oBy, oSort).paginate(page, perPage)

      if (result.rows.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async show(id) {
    try {
      const result = await Product
      .query()
      .whereRaw('products.id = ? ', [id])
      .with('unity')
      // .with('weight')
      // .with('auxiliaryCode')
      .with('images')
      .with('attributesProduct', (builder) => {
        builder.select('id', 'name', 'type_id')
        .with('type', (builder) => {
          builder.select('id', 'id_unique', 'public', 'required', 'style_id', 'name')
          .with('style')
        })
      })
      .with('sectionProduct', (builder) => {
        builder.select('id', 'name', 'type_id')
        .with('type', (builder) => {
          builder.select('id', 'required', 'id_unique', 'name')
        })
      })
      .with('created', (builder) => {
        builder.select('id', 'username', 'partner_id')
        .with('partner', (builder) => {
          builder.select('id', 'name_reason')
        })
      })
      .with('updated', (builder) => {
        builder.select('id', 'username', 'partner_id')
        .with('partner', (builder) => {
          builder.select('id', 'name_reason')
        })
      })
      .first()

      if (result === null) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async indexProductsSection(idSection, params) {
    try {
      const wBy = params.wBy ? params.wBy : 'id'
      const w = params.w ? params.w : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let active = params.active ? params.active : null
      if (active == 'true') { active = 1 } else if (active == 'false') { active = 0 }

      const section = await Section.findOrFail(idSection)
      const products = await section.products()
        .whereRaw('products.' + wBy + ' like ? ', ['%' + w + '%'])
        .where(function () {
          if (active == 1 || active == 0) this.where('active', active)
        })
        .with('images')
        //.with('auxiliaryCode')
        .with('attributesProduct', (builder) => {
          builder.select('id', 'name', 'type_id')
            .with('type', (builder) => {
              builder.select('id', 'id_unique', 'public', 'style_id', 'required', 'name')
              .with('style')
            })
        })
        .with('sectionProduct', (builder) => {
          builder.select('id', 'name', 'type_id').where('product_section.id', 1)
            .with('type', (builder) => {
              builder.select('id', 'id_unique', 'required', 'name')
            })
        })
        .with('created', (builder) => {
          builder.select('id', 'username', 'partner_id')
            .with('partner', (builder) => {
              builder.select('id', 'name_reason')
            })
        })
        .with('updated', (builder) => {
          builder.select('id', 'username', 'partner_id')
            .with('partner', (builder) => {
              builder.select('id', 'name_reason')
            })
        })
        .with('unity')
        .orderBy(oBy, oSort)
        .paginate(page, perPage)

      if (products.rows.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, products)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new ProductService()

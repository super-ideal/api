'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const Image = use('App/Models/Product/Image')

class ImageService {
  async index(params, idProduct) {
    try {
      const wBy = params.wBy ? params.wBy : 'filename'
      const w = params.w ? params.w : ''
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const active = params.active ? (JSON.parse(params.active) == null ? null : +JSON.parse(params.active)) : null
      const pagination = params.pagination = params.pagination ? JSON.parse(params.pagination) : false
      params.detailsProduct = params.detailsProduct ? JSON.parse(params.detailsProduct) : false

      let query = Image.query().whereRaw(wBy + ' like ? and product_id = ?', ['%' + w + '%', idProduct])
      if (active != null)
        query = query.whereRaw('active = ?', [active])

      if (params.detailsProduct) {
        query = query.with('product')
      }

      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.length === 0)
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async show(id) {
    try {
      const result = await Image.findOrFail(id)
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(data, idProduct) {
    try {
      let image = data
      image['product_id'] = idProduct

      const result = await Image.create(image)
      return new Response('Imagem CADASTRADA com sucesso!', 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(data, idImage) {
    try {
      const result = await Image.findOrFail(idImage)
      result.merge(data)
      await result.save()

      return new Response("Imagem ATUALIZADA com sucesso!", 200, result)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async delete(id) {
    try {
      const result = await Image.findOrFail(id)
      await result.delete()

      return new Response('Imagem EXCLUÍDA com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new ImageService()

'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const Weight = use('App/Models/Product/Weight')

class WeightService {
  async show(product_id) {
    try {
      const result = await Weight.findByOrFail({product_id: product_id})
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(data) {
    try {
      const result = await Weight.create(data)
      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(data, product_id) {
    try {
      const result = await Weight.findByOrFail({product_id: product_id})
      result.merge(data)
      await result.save()

      return new Response(null, 200, result)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new WeightService()

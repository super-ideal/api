'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')
const Database = use('Database')

const Product = use('App/Models/Product/Product')

class ProductService {
  async index(params) {
    try {
      const wBy = params.wBy ? params.wBy : 'id'
      const w = params.w ? params.w : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let active = params.active ? params.active : null
      if (active == 'true') { active = 1 } else if (active == 'false') { active = 0 }

      const result = await Product
        .query()
        .whereRaw('products.' + wBy + ' like ? ', ['%'+w+'%'])
        .where(function () {
          if (active == 1 || active == 0) this.where('active', active)
        })
        .with('auxiliaryCode')
        .with('attributesProduct', (builder) => {
          builder.select('id', 'name', 'type_id')
          .with('type', (builder) => {
            builder.select('id', 'id_unique', 'public', 'style_id', 'required', 'name')
            .with('style')
          })
        })
        .with('sectionProduct', (builder) => {
          builder.select('id', 'name', 'type_id')
          .with('type', (builder) => {
            builder.select('id', 'id_unique', 'required', 'name')
          })
        })
        .with('created', (builder) => {
          builder.select('id', 'username', 'partner_id')
          .with('partner', (builder) => {
            builder.select('id', 'name_reason')
          })
        })
        .with('updated', (builder) => {
          builder.select('id', 'username', 'partner_id')
          .with('partner', (builder) => {
            builder.select('id', 'name_reason')
          })
        })
        .with('unity')
        .with('images')
        .orderBy(oBy, oSort)
        .paginate(page, perPage)

      if (result.rows.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async show(id) {
    try {
      const result = await Product
      .query()
      .whereRaw('products.id = ? ', [id])
      .with('unity')
      // .with('weight')
      // .with('auxiliaryCode')
      .with('images')
      .with('attributesProduct', (builder) => {
        builder.select('id', 'name', 'type_id')
        .with('type', (builder) => {
          builder.select('id', 'id_unique', 'public', 'required', 'style_id', 'name')
          .with('style')
        })
      })
      .with('sectionProduct', (builder) => {
        builder.select('id', 'name', 'type_id')
        .with('type', (builder) => {
          builder.select('id', 'required', 'id_unique', 'name')
        })
      })
      .with('created', (builder) => {
        builder.select('id', 'username', 'partner_id')
        .with('partner', (builder) => {
          builder.select('id', 'name_reason')
        })
      })
      .with('updated', (builder) => {
        builder.select('id', 'username', 'partner_id')
        .with('partner', (builder) => {
          builder.select('id', 'name_reason')
        })
      })
      .first()

      if (result === null) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(data, auth) {
    try {
      const user = await auth.getUser()
      data['product']['created_by'] = user.id

      const product = await Product.create(data.product)
      if (data.section != undefined)
        await product.sectionProduct().attach(await this.organizaDados(data.section))

      if (data.attributes != undefined)
        await product.attributesProduct().attach(await this.organizaDados(data.attributes))

      if (data.images != undefined)
        await product.images().createMany(data.images)

      return new Response(null, 200, product)
    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(data, id, auth) {
    try {
      const user = await auth.getUser()
      const product = await Product.findOrFail(id)
      const priceOld = product['price']

      data['product']['updated_by'] = user.id
      product.merge(data.product)
      await product.save()

      if (data.section != undefined)
        await product.sectionProduct().sync(await this.organizaDados(data.section))

      if (data.attributes != undefined)
        await product.attributesProduct().sync(await this.organizaDados(data.attributes))

      if (data.images != undefined)
        await product.images().createMany(data.images)

      // Atualiza todos do mesmo grupo de preço
      if (data['product']['price'] != undefined && data['product']['price'] != priceOld) {
        await Database
          .table('products as p')
          .innerJoin('product_attributes_products as pap', 'pap.product_id', 'p.id')
          .innerJoin('product_attributes as pa', 'pa.id', 'pap.attribute_id')
          .innerJoin('product_attributes_types as pat', 'pat.id', 'pa.type_id')
          .whereRaw('pat.id_unique = ?', ['price_group'])
          .update({'p.price': data['product']['price'], updated_by: data['product']['updated_by']})
      }

      return new Response('Dados atualizados com sucesso', 200, product)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Verifica se um determinado código de barras já está cadastrado
   * @param {*} barcode Código de barras (EAN)
   */
  async verifyBarcodeDuplicated(barcode) {
    try {
      const result = await Database
        .table('products as p')
        .leftJoin('product_auxiliary_codes as pac', 'pac.product_id', 'p.id')
        .whereRaw('p.barcode = ? OR pac.code = ?', [barcode, barcode])

      if (result.length == 0) {
        return new Response('O Código de Barras não existe', 200, null)
      } else {
        return new Response('O Código de Barras já está cadastrado', 422, null)
      }
    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca os produtos de um grupo de preço
   * @param {*} params Parâmetros para a busca (column, operator, text, order by, sort)
   * @param {*} idAttributeProduct ID da tabela onde fica o ID do produto e o ID do atributo do grupo de preço
   */
  async productsGroupPrice(params, idAttributeProduct) {
    try {
      const column = params.column ? params.column : 'p.description'
      const operator = params.operator ? params.operator : 'anywhere'
      const text = params.text ? params.text : ''
      const oBy = params.oBy ? params.oBy : 'p.description'
      const oSort = params.oSort ? params.oSort : 'asc'

      let query = Database
        .table('products AS p')
        .select('p.id', 'p.description', 'p.barcode')
        .innerJoin('product_attributes_products AS pap', 'pap.product_id', 'p.id')
        .innerJoin('product_attributes AS pa', 'pa.id', 'pap.attribute_id')
        .whereRaw('pa.id = ?', [idAttributeProduct])

      if (operator == 'anywhere')
        query = query.whereRaw(column + ' like ?', ['%'+text+'%'])
      else if (operator == 'equal')
        query = query.whereRaw(column + ' = ?', [text])
      else if (operator == 'different')
        query = query.whereRaw(column + ' != ?', [text])

      const result =  await query.orderBy(oBy, oSort);

      if (result.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async organizaDados(data) {
    const array = [];
    for(let i of Object.keys(data)) {
      if (Array.isArray(data[i])) {

        for(let a of data[i]) {
          if (data[i] !== null) {
            array.push(a['id'])
          }
        }

      } else {
        if (data[i] !== null) {
          array.push(data[i]['id'])
        }
      }
    }

    return array;
  }
}

module.exports = new ProductService()

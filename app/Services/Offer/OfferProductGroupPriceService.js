'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')
const Database = use('Database')

class OfferProductGroupPriceService {
  /**
   * Busca os produtos do grupo de preço que estão em oferta
   * @param {*} idOffer ID da agenda de oferta
   * @param {*} idOfferProduct ID da tabela de relacionamento do produto com a agenda de oferta
   */
  async index(idOffer, idOfferProduct) {
    try {
      let query = Database
        .select('opgp.*', 'op.product_id as parent_product')
        .table('offer_products_group_price as opgp')
        .innerJoin('offer_products as op', 'op.id', 'opgp.offer_product_id')
        .innerJoin('products as p', 'p.id', 'opgp.product_id')
        .where({'p.active': 1})

      if (idOffer && idOfferProduct)
        query = query.whereRaw('opgp.offer_id = ? and opgp.offer_product_id = ?', [idOffer, idOfferProduct])
      else
        query = query.whereRaw('opgp.offer_id = ?', [idOffer])

      const result = await query

      if (result.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new OfferProductGroupPriceService()

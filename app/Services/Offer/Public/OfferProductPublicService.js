'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')

const OfferProductProgress = use('App/Models/Offer/Public/OfferProductProgress')

class OfferProductPublicService {
  /**
   * Verifica se um determinado produto está em alguma agenda de oferta que está em andamento
   * @param {*} params Parâmetros de filtro de busca (column, operator, text)
   */
  async productsOffersProgress(params) {
    try {
      const column = params.column ? params.column : 'product_description'
      const operator = params.operator ? params.operator : 'anywhere'
      const text = params.text ? params.text : ''

      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const pagination = params.pagination ? JSON.parse(params.pagination) : false

      let query = OfferProductProgress.query()

      if (operator == 'anywhere')
        query = query.whereRaw(column + ' like ?', ['%'+text+'%'])
      else if (operator == 'started')
        query = query.whereRaw(column + ' like ?', [text+'%'])
      else if (operator == 'finished')
        query = query.whereRaw(column + ' like ?', ['%'+text])
      else if (operator == 'equal')
        query = query.whereRaw(column + ' = ?', [text])
      else if (operator == 'different')
        query = query.whereRaw(column + ' != ?', [text])
      else if (operator == 'smaller')
        query = query.whereRaw(column + ' < ?', [text])
      else if (operator == 'bigger')
        query = query.whereRaw(column + ' > ?', [text])
      else if (operator == 'contained')
        query = query.whereIn(column, text.split(','))

      // query = query.select('offer_id_unique', 'datetime_end', 'product_id', 'offer_product_id')

      query = query
        .with('images')
        .with('unity_product')

      if (pagination)
        query = query.paginate(page, perPage)
      else
        query = query.fetch()

      const result = await query

      if (result.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new OfferProductPublicService()

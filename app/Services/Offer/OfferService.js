'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')
const Database = use('Database')

const Offer = use('App/Models/Offer/Offer')

class OfferService {
  async index(params) {
    try {
      const column = params.column ? params.column : 'name'
      const operator = params.operator ? params.operator : 'anywhere'
      const text = params.text ? params.text : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const gBy = params.gBy ? params.gBy : null
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let query = Database.table('offer_view')

      if (operator == 'anywhere')
        query = query.whereRaw(column + ' like ?', ['%'+text+'%'])
      else if (operator == 'started')
        query = query.whereRaw(column + ' like ?', [text+'%'])
      else if (operator == 'finished')
        query = query.whereRaw(column + ' like ?', ['%'+text])
      else if (operator == 'equal')
        query = query.whereRaw(column + ' = ?', [text])
      else if (operator == 'different')
        query = query.whereRaw(column + ' != ?', [text])
      else if (operator == 'smaller')
        query = query.whereRaw(column + ' < ?', [text])
      else if (operator == 'bigger')
        query = query.whereRaw(column + ' > ?', [text])
      else if (operator == 'contained')
        query = query.whereIn(column, text.split(','))

      if (gBy !== null) {
        query = query.groupBy(params.gBy)
      }

      const result = await query.orderBy(oBy, oSort).paginate(page, perPage);

      if (result.data.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

      /*let result;
      if (operator == 'between') {
        result = await Offer.query()
        .whereBetween(column,[text,textBetween])
        .with('offerType', (builder) => {
          builder.select('id', 'description')
        })
        .with('created', (builder) => {
          builder.select('id', 'partner_id', 'email')
          .with('partner', (builder) => {
            builder.select('id', 'name_reason')
          })
        })
        .with('updated', (builder) => {
          builder.select('id', 'partner_id', 'email')
          .with('partner', (builder) => {
            builder.select('id', 'name_reason')
          })
        })
        .paginate(page, perPage)
      } else {
        if (operator == 'like') {
          text = `%${text}%`;
        }

        result = await Offer.query()
          .whereRaw(`${column} ${operator} ?`, [text])
          .with('offerType', (builder) => {
            builder.select('id', 'description')
          })
          .with('created', (builder) => {
            builder.select('id', 'partner_id', 'email')
            .with('partner', (builder) => {
              builder.select('id', 'name_reason')
            })
          })
          .with('updated', (builder) => {
            builder.select('id', 'partner_id', 'email')
            .with('partner', (builder) => {
              builder.select('id', 'name_reason')
            })
          })
          .paginate(page, perPage)
      }*/

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async show(id) {
    try {
      const result = await Database
        .table('offer_view')
        .whereRaw('id = ?', [id])
        .first()

      /*const result = await Offer
      .query()
      .whereRaw('id = ?', [id])
      .with('offerType', (builder) => { builder.select('id', 'description') })
      .with('created', (builder) => {
        builder.select('id', 'partner_id', 'username')
        .with('partner', (builder) => {
          builder.select('id', 'name_reason')
        })
      })
      .with('updated', (builder) => {
        builder.select('id', 'partner_id', 'username')
        .with('partner', (builder) => {
          builder.select('id', 'name_reason')
        })
      })
      .first()*/

      if (result === undefined) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async store(data, auth) {
    try {
      const user = await auth.getUser()
      data['created_by'] = user.id

      const offer = await Offer.create(data)

      return new Response(null, 200, offer)
    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async update(data, id, auth) {
    try {
      const user = await auth.getUser()
      const result = await Offer.findOrFail(id)

      data['updated_by'] = user.id
      result.merge(data)
      await result.save()

      return new Response("Agenda de Oferta ATUALIZADA com sucesso!", 200, result)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async delete(id) {
    try {
      const result = await Offer.findOrFail(id)
      await result.delete()

      return new Response('Agenda de Oferta EXCLUÍDA com sucesso!', 200, null)

    } catch (error) {
        throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new OfferService()

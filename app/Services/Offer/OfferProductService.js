'use strict'

const ErrorException = use('App/Exceptions/ErrorException')
const Response = use('App/Class/ResponseClass')
const Database = use('Database')

const OfferProduct = use('App/Models/Offer/OfferProduct')

class OfferProductService {
  /**
   * Verifica se um determinado produto está em alguma agenda de oferta que está em andamento
   * @param {*} params Parâmetros de filtro de busca (column, operator, text)
   */
  async productsOffersProgress(params) {
    try {
      const column = params.column ? params.column : 'offer_name'
      const operator = params.operator ? params.operator : 'anywhere'
      const text = params.text ? params.text : ''

      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const pagination = params.pagination ? JSON.parse(params.pagination) : false

      let query = Database.table('offer_products_progress_view')

      if (operator == 'anywhere')
        query = query.whereRaw(column + ' like ?', ['%'+text+'%'])
      else if (operator == 'started')
        query = query.whereRaw(column + ' like ?', [text+'%'])
      else if (operator == 'finished')
        query = query.whereRaw(column + ' like ?', ['%'+text])
      else if (operator == 'equal')
        query = query.whereRaw(column + ' = ?', [text])
      else if (operator == 'different')
        query = query.whereRaw(column + ' != ?', [text])
      else if (operator == 'smaller')
        query = query.whereRaw(column + ' < ?', [text])
      else if (operator == 'bigger')
        query = query.whereRaw(column + ' > ?', [text])
      else if (operator == 'contained')
        query = query.whereIn(column, text.split(','))

      if (pagination)
        query = query.paginate(page, perPage)

      const result = await query

      if (result.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Verifica se um determinado produto está em alguma agenda de oferta que está a iniciar ou em andamento
   * @param {*} params Parâmetros de filtro de busca (column, operator, text)
   */
  async verifyProductsInOffer(params) {
    try {
      const column = params.column ? params.column : 'offer_name'
      const operator = params.operator ? params.operator : 'anywhere'
      const text = params.text ? params.text : ''

      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20
      const pagination = params.pagination ? JSON.parse(params.pagination) : false

      let query = Database
        .table('offer_products_price_group_view as oppgv')
        .select('oppgv.*', 'ov.status', 'ov.datetime_start', 'ov.datetime_end', 'ov.name', 'ov.offer_type_name')
        .innerJoin('offer_view as ov', 'ov.id', 'oppgv.offer_id')
        .whereRaw('oppgv.offer_product_id = ? and ov.active = ? and oppgv.product_active = ? and ov.datetime_end >= now()', [1, 1, 1])

      if (operator == 'anywhere')
        query = query.whereRaw(column + ' like ?', ['%'+text+'%'])
      else if (operator == 'started')
        query = query.whereRaw(column + ' like ?', [text+'%'])
      else if (operator == 'finished')
        query = query.whereRaw(column + ' like ?', ['%'+text])
      else if (operator == 'equal')
        query = query.whereRaw(column + ' = ?', [text])
      else if (operator == 'different')
        query = query.whereRaw(column + ' != ?', [text])
      else if (operator == 'smaller')
        query = query.whereRaw(column + ' < ?', [text])
      else if (operator == 'bigger')
        query = query.whereRaw(column + ' > ?', [text])
      else if (operator == 'contained')
        query = query.whereIn(column, text.split(','))

      if (pagination)
        query = query.paginate(page, perPage)

      const result = await query

      if (result.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca todos os produtos das agendas de ofertas
   * @param {*} params Parâmetros para busca dos dados
   * @param {*} idOffer ID da agenda de oferta
   */
  async index(params, idOffer) {
    try {
      const viewGP = params.viewGP ? params.viewGP : false
      const column = params.column ? params.column : 'product_description'
      const operator = params.operator ? params.operator : 'anywhere'
      const text = params.text ? params.text : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const gBy = params.gBy ? params.gBy : null
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let table
      if (JSON.parse(viewGP)) {
        table = 'offer_products_price_group_view'
      } else {
        table = 'offer_products_view'
      }

      let query = Database.table(table).whereRaw('offer_id = ?', [idOffer])

      if (operator == 'anywhere')
        query = query.whereRaw(column + ' like ?', ['%'+text+'%'])
      else if (operator == 'started')
        query = query.whereRaw(column + ' like ?', [text+'%'])
      else if (operator == 'finished')
        query = query.whereRaw(column + ' like ?', ['%'+text])
      else if (operator == 'equal')
        query = query.whereRaw(column + ' = ?', [text])
      else if (operator == 'different')
        query = query.whereRaw(column + ' != ?', [text])
      else if (operator == 'smaller')
        query = query.whereRaw(column + ' < ?', [text])
      else if (operator == 'bigger')
        query = query.whereRaw(column + ' > ?', [text])
      else if (operator == 'contained')
        query = query.whereIn(column, text.split(','))

      if (gBy !== null) {
        query = query.groupBy(params.gBy)
      }

      const result = await query.orderBy(oBy, oSort).paginate(page, perPage);

			/*const result = await OfferProduct
        .query()
        .whereRaw('offer_id = ?', [idOffer])
        .where(function () {
          if (active == 1 || active == 0) this.where('active', active)
        })
        .with('product')
        .with('added', (builder) => {
          builder.select('id', 'username', 'partner_id')
          .with('partner', (builder) => {
            builder.select('id', 'name_reason')
          })
        })
        .with('updated', (builder) => {
          builder.select('id', 'username', 'partner_id')
          .with('partner', (builder) => {
            builder.select('id', 'name_reason')
          })
        })
        .with('offerMotive')
        .orderBy(oBy, oSort)
        .paginate(page, perPage)*/

      if (result.data.length === 0) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca os detalhes de um produto de uma agenda de oferta
   * @param {*} idOfferProduct ID da tabela que contem os produtos das agendas de oferta
   */
  async show(idOfferProduct) {
    try {
			const result = await OfferProduct
        .query()
        .whereRaw('id = ?', [idOfferProduct])
        .with('added', (builder) => {
          builder.select('id', 'username', 'partner_id')
        })
        .with('updated', (builder) => {
          builder.select('id', 'username', 'partner_id')
        })
        .with('offerMotive', (builder) => {
          builder.select('id', 'description')
        })
        .first()

      if (result === null) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)

    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Salva os dados de um produto de uma determinada agenda de oferta
   * @param {*} data Objeto com os dados que serão salvos
   * @param {*} idOffer ID da agenda de oferta
   * @param {*} auth Dados de autenticação do usuário
   */
  async store(data, idOffer, auth) {
    try {
      const user = await auth.getUser()
      data['product']['added_by'] = user.id
      data['product']['offer_id'] = idOffer

      const result = await OfferProduct.create(data['product'])
      const offer_product = result.toJSON()

      // Grava os produtos que vão estar no Grupo de Preço
      if (data['product']['price_group'] && data['products_price_group']['addProductsGroupPrice'] != undefined) {
        const addProductsGroupPrice = []

        // Faz tratamento dos dados
        for (var product of data['products_price_group']['addProductsGroupPrice']) {
          addProductsGroupPrice.push({
            offer_id: offer_product['offer_id'],
            offer_product_id: offer_product['id'],
            product_id: product['id']
          });
        }

        await result.productPriceGroup().createMany(addProductsGroupPrice)
      }

      // Remove os produtos que não irão estar no Grupo de Preço (update)
      if (data['product']['price_group'] && data['products_price_group']['rmProductsGroupPrice'] != undefined) {
        const rmProductsGroupPrice = []

        // Faz tratamento dos dados
        for (var product of data['products_price_group']['rmProductsGroupPrice']) {
          rmProductsGroupPrice.push(product['id']);
        }

        await result.productPriceGroup().where('offer_id', offer_product['offer_id']).whereIn('product_id', rmProductsGroupPrice).delete()
      }

      return new Response("Produto ADICIONADO com sucesso!", 200, result)
    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Atualiza os dados de um produto de uma determinada agenda de oferta
   * @param {*} data Objeto com os dados que serão atualizados
   * @param {*} idOfferProduct ID da tabela que contem os produtos das agendas de oferta
   * @param {*} auth Dados de autenticação do usuário
   */
  async update(data, idOfferProduct, auth) {
    try {
      const user = await auth.getUser()
      const result = await OfferProduct.findOrFail(idOfferProduct)

      // Adiciona o ID do usuário que está atualizando
      data['product']['updated_by'] = user.id

      // Faz o merge dos dados e salva no BD
      result.merge(data['product'])
      await result.save()

      const offer_product = result.toJSON()

      // Grava os produtos que vão estar no Grupo de Preço
      if (data['product']['price_group'] && data['products_price_group']['addProductsGroupPrice'] != undefined) {
        const addProductsGroupPrice = []

        // Faz tratamento dos dados
        for (var product of data['products_price_group']['addProductsGroupPrice']) {
          addProductsGroupPrice.push({
            offer_id: offer_product['offer_id'],
            offer_product_id: offer_product['id'],
            product_id: product['id']
          });
        }

        await result.productPriceGroup().createMany(addProductsGroupPrice)
      }

      // Remove os produtos que não irão estar no Grupo de Preço (update)
      if (data['product']['price_group'] && data['products_price_group']['rmProductsGroupPrice'] != undefined) {
        const rmProductsGroupPrice = []

        // Faz tratamento dos dados
        for (var product of data['products_price_group']['rmProductsGroupPrice']) {
          rmProductsGroupPrice.push(product['id']);
        }

        await result.productPriceGroup().where('offer_id', offer_product['offer_id']).whereIn('product_id', rmProductsGroupPrice).delete()
      }

      return new Response("Produto ATUALIZADO com sucesso!", 200, result)
    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Exclui um produto de uma agenda de oferta
   * @param {*} idOfferProduct ID da tabela que contem os produtos das agendas de oferta
   */
  async delete(idOfferProduct) {
    try {
      const product = await OfferProduct.findOrFail(idOfferProduct)
      await product.delete()

      return new Response('Produto EXCLUÍDO com sucesso!', 200, null)
    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Exclui todos os produtos de uma agenda de oferta
   * @param {*} idOffer ID da agenda de oferta
   */
  async clear(idOffer) {
    try {
      await OfferProduct.query().where('offer_id', idOffer).delete()

      return new Response('Produtos EXCLUÍDOS com sucesso!', 200, null)
    } catch (error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }
}

module.exports = new OfferProductService()

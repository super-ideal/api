'use strict'
const Hash = use('Hash')
const Database = use('Database')
const Response = use('App/Class/ResponseClass')
const ErrorException = use('App/Exceptions/ErrorException')

const User = use('App/Models/User/User')
const UserView = use('App/Models/User/UserView')

class UserService {
  /**
   * Busca todos os usuários
   * @param {*} params Parâmetros para busca dos dados
   */
  async index (params) {
    try {
      const column = params.column ? params.column : 'email'
      const operator = params.operator ? params.operator : 'anywhere'
      const text = params.text ? params.text : ''
      const oBy = params.oBy ? params.oBy : 'id'
      const oSort = params.oSort ? params.oSort : 'asc'
      const gBy = params.gBy ? params.gBy : null
      const page = params.page ? parseInt(params.page, 10) : 1
      const perPage = params.perPage ? (parseInt(params.perPage, 10) < 100 ? parseInt(params.perPage, 10) : 100) : 20

      let query = UserView.query()

      if (operator == 'anywhere')
        query = query.whereRaw(column + ' like ?', ['%'+text+'%'])
      else if (operator == 'started')
        query = query.whereRaw(column + ' like ?', [text+'%'])
      else if (operator == 'finished')
        query = query.whereRaw(column + ' like ?', ['%'+text])
      else if (operator == 'equal')
        query = query.whereRaw(column + ' = ?', [text])
      else if (operator == 'different')
        query = query.whereRaw(column + ' != ?', [text])
      else if (operator == 'smaller')
        query = query.whereRaw(column + ' < ?', [text])
      else if (operator == 'bigger')
        query = query.whereRaw(column + ' > ?', [text])
      else if (operator == 'contained')
        query = query.whereIn(column, text.split(','))

      if (gBy !== null) {
        query = query.groupBy(params.gBy)
      }

      const result = await query.orderBy(oBy, oSort).paginate(page, perPage);

      if (!result) {
        throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
      }

      return new Response(null, 200, result)
    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  /**
   * Busca os detalhes de um usuários
   * @param {*} id ID da tabela que contem os usuários
   */
  async showUser (idUser) {
    try {
      const user = await User
        .query()
        .whereRaw('id = ?', [idUser])
        .with('partner')
        .first()

        if (!user) {
          throw new ErrorException('Não foi encontrado nenhum resultado com os dados informados', 404, 'E_MISSING_DATABASE_ROW', 'ErrorException')
        }

      return new Response(null, 200, user)

    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async storeUser (userData) {
    try {
      const user = await User.create(userData)
      return new Response(null, 201, user)

    } catch(error) {
      switch (error.code) {
        case 'ER_DUP_ENTRY': error.message = 'A pessoa informada já esta relacionada a outro usuário'; break
        case 'ER_NO_REFERENCED_ROW_2': error.message = 'A pessoa informada não foi encontrada'; break
      }

      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async updateUser (data, idUser) {
    try {
      const user = await User.findOrFail(idUser)

      if (data.password) {
        data.password = await Hash.make(data.password)
      } else {
        delete data.password
      }

      user.merge(data)
      await user.save()

      return new Response('Dados atualizados com sucesso', 200)

    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async permissionOne (idUser, namePage, nameAction) {
    try {
      const pageAction = await Database
        .select('acl_page_actions.id').from('acl_page_actions')
        .innerJoin('acl_pages', 'acl_page_actions.page_id', 'acl_pages.id').where('acl_pages.name', namePage)
        .innerJoin('acl_actions', 'acl_page_actions.action_id', 'acl_actions.id').where('acl_actions.name', nameAction)
        .first()

      if (pageAction == undefined) throw new ErrorException('A Página e Ação informada, não tem relacionamentos. Contate o Administrador!', 404)

      const aclUser = await Database
        .connection('mysql')
        .select('id', 'user_id')
        .from('acl_page_action_users')
        .where({user_id: idUser, page_action_id: pageAction['id']})
        .first()

      if (aclUser == undefined) throw new ErrorException('Usuário sem permissão de acesso', 403)

      return new Response('Usuário com permissão de acesso', 200, aclUser)

    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

  async permissionMany (idUser) {
    try {
      const aclUser = await User
        .query()
        .select('id', 'email')
        .where('id', '=', idUser)
        .with('pageActionUser.page', (builder) => {
          builder.select('id', 'name', 'description')
        })
        .with('pageActionUser.action', (builder) => {
          builder.select('id', 'name', 'description')
        })
        .fetch()

      return new Response(null, 200, aclUser)

    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

}

module.exports = new UserService()

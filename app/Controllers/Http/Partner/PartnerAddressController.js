'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const PartnerAddressService = use('App/Services/Partner/PartnerAddressService')

class PartnerAddressController {
  async index({ request, response }) {
    const result = await PartnerAddressService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await PartnerAddressService.show(params.id)
    return response.status(result.status).json(result)
  }

  async store({ request, response, params }) {
    const result = await PartnerAddressService.store(request.post(), params.idPartner)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response, auth }) {
    const data = request.all()

    const result = await PartnerAddressService.update(data, params.id, auth)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await PartnerAddressService.delete(params.id)
    return response.status(result.status).json(result)
  }
}

module.exports = PartnerAddressController

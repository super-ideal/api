'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const PartnerEmailService = use('App/Services/Partner/PartnerEmailService')

class PartnerEmailController {
  async index({ request, response }) {
    const result = await PartnerEmailService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await PartnerEmailService.show(params.id)
    return response.status(result.status).json(result)
  }

  async store({ params, request, response }) {
    const result = await PartnerEmailService.store(request.post(), params.idPartner)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response, auth }) {
    const data = request.all()

    const result = await PartnerEmailService.update(data, params.id, auth)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await PartnerEmailService.delete(params.id)
    return response.status(result.status).json(result)
  }
}

module.exports = PartnerEmailController

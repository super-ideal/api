'use strict'

const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const AuthService = use('App/Services/Auth/AuthService')

//const Database = use('Database')
//const AuthSocial = use('App/Models/AuthSocial')
//const ErrorException = use('App/Exceptions/ErrorException')
//const Encryption = use('Encryption')

class AuthController {
  async postLogin ({ request, response, auth }) {
    const result = await AuthService.login(auth, request.all())
    return response.status(result.status).json(result)
  }

  async register ({ request, response, auth }) {
    let data = request.all();
    let obj = {
      email: data.user.email,
      password: data.user.password,
      password_confirmation: data.user.password_confirmation,
      cpf_cnpj: data.partner.cpf_cnpj,
      name_reason: data.partner.name_reason,
      type_person: data.partner.type_person
    }

    const rules = {
      email: 'unique:users',
      password: 'required|confirmed|min:6',
      cpf_cnpj: 'unique:partners',
      name_reason: 'required',
    }

    const messages = {
      'email.unique': 'O e-mail informado já está cadastrado',
      'password.min': 'O tamanho mínimo da senha é de 6 caracteres',
      'password.required': 'Uma senha deve ser informada',
      'password.confirmed': 'A senha de confirmação não é válida',
      'cpf_cnpj.unique': 'O CPF/CNPJ informado está já está cadastrado',
      'name_reason.required': 'O nome ou razão social é obrigatório',
    }

    const validation = await validateAll(obj, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    delete data['user']['password_confirmation']
    const result = await AuthService.register(data, auth)
    return response.status(result.status).json(result)
  }

  async postRefreshToken({ request, response, auth }) {
    // Validation
    const rules = {
      refresh_token: 'required'
    }

    const messages = {
      'refresh_token.required': 'Você deve informar o refresh token'
    }

    const validation = await validateAll(request.all(), rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Refresh
    const result = await AuthService.refreshToken(auth, request.input('refresh_token'))
    return response.status(result.status).json(result)
  }

  async getLogoutAll ({ response, auth }) {
    const result = await AuthService.logoutAll(auth)
    return response.status(result.status).json(result)
  }

  async getTokens ({ response, auth }) {
    const result = await AuthService.getTokens(auth)
    return response.status(result.status).json(result)
  }

  async getUserLogged({ response, auth }) {
    const result = await AuthService.getUserLogged(auth)
    return response.status(result.status).json(result)
  }

  async getName({ response, request }) {
    const data = request.all()
    const result = await AuthService.getName(data)
    return response.status(result.status).json(result)
  }

  /*async getProviderRedirect ({ ally, params }) {
    await ally.driver(params.provider).redirect()
  }

  async getProviderCallback ({ ally, params, auth, response }) {
    const provider = params.provider

    try {
      const userData = await ally.driver(provider).getUser()

      try {
        const authUser = await AuthSocial.query().where({
          'provider': provider,
          'provider_id': userData.getId()
        }).first()

        // Usuário já está cadastrado (faz o login JWT)
        if (!(authUser === null)) {
          const user = await Database.connection('mysql')
            .select('username', 'email', 'password')
            .from('users')
            .where('id', authUser.id)

          console.log(user.password)

          const decryptedPassword = Encryption.decrypt(user.password)
          console.log(decryptedPassword)

          const result = await AuthService.login(auth, user.email, decryptedPassword)
          return response.status(result.status).json(result)
        }

        return response.status(200).send({ opa: userData })

      } catch (error) {
        return response.status(error.status).send({ error: error })
      }

      return response.status(200).send({
        original: userData.getOriginal(),
        profile: userData.toJSON(),
        email: userData.getEmail(),
        name: userData.getName(),
        username: userData.getNickname(),
        login_source: provider
      })

    } catch (error) {
      //if (error.original.statusCode === 401) throw new ErrorException(error.original.data, error.original.statusCode, error.code)

      return response.status(error.status).send({ error: error })
    }
  }*/

  /*async postNewRefreshToken({request, response, auth}) {
    // Validation
    const rules = { refresh_token: 'required' }
    const messages = { 'refresh_token.required': 'Você deve informar o refresh token' }
    const validation = await validateAll(request.all(), rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Refresh
    try {
      const refreshToken = request.input('refresh_token')
      return await auth.newRefreshToken().generateForRefreshToken(refreshToken)

    } catch (error) {
      response.status(401).send({ message: 'Não foi possivel gerar um novo token' })
    }
  }*/

  /*async postLogout ({ response, request, auth }) {
    // Validation
    const rules = { refresh_token: 'required' }
    const messages = { 'refresh_token.required': 'Você deve informar o refresh token' }
    const validation = await validateAll(request.all(), rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Logout
    try {
      const user = auth.current.user
      const refreshToken = request.input('refresh_token')
      await user
        .tokens()
        .where('type', 'jwt_refresh_token')
        .where('token', Encryption.decrypt(refreshToken))
        .delete()

      return response.send({ message: 'Você saiu do sistema com sucesso' })

    } catch (error) {
      return response.status(401).send({ message: 'Ocorreu um erro ao sair do sistema', data: error })
    }
  }*/
}

module.exports = AuthController

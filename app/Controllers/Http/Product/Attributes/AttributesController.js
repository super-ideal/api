'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const AttributesService = use('App/Services/Product/Attributes/AttributesService')

class AttributesController {
  async searchAttributesByTypes({ params, response }) {
    const result = await AttributesService.searchAttributesByTypes(params.idType)
    return response.status(result.status).json(result)
  }

  async index({ request, response }) {
    const result = await AttributesService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await AttributesService.show(params.idAttribute)
    return response.status(result.status).json(result)
  }

  async store({ request, response }) {
    const data = request.all()

    const rules = {
      name: 'required',
      type_id: 'required',
    }

    const messages = {
      'name.required': 'Um Nome deve ser informado',
      'type_id.required': 'Deve informar o ID do Tipo',
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    const result = await AttributesService.store(data)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response }) {
    const data = request.all()

    const result = await AttributesService.update(data, params.idAttribute)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await AttributesService.delete(params.idAttribute)
    return response.status(result.status).json(result)
  }
}

module.exports = AttributesController

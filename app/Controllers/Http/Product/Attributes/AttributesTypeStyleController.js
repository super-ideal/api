'use strict'
const AttributesTypeStyleService = use('App/Services/Product/Attributes/AttributesTypeStyleService')

class AttributesTypeStyleController {
  async index({ response }) {
    const result = await AttributesTypeStyleService.index()
    return response.status(result.status).json(result)
  }
}

module.exports = AttributesTypeStyleController

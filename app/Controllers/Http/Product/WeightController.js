'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const WeightService = use('App/Services/Product/WeightService')

class WeightController {
  async show({ params, response }) {
    const result = await WeightService.show(params.idProduct)
    return response.status(result.status).json(result)
  }

  async store({ request, response }) {
    const data = request.all()

    const rules = {
      product_id: 'required'
    }

    const messages = {
      'product_id.required': 'O ID do produto deve ser informado'
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Salva
    const result = await WeightService.store(data)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response }) {
    const data = request.all()

    // Atualiza
    const result = await WeightService.update(data, params.idProduct)
    return response.status(result.status).json(result)
  }
}

module.exports = WeightController

'use strict'
const SectionTypePublicService = use('App/Services/Product/Section/Public/SectionTypePublicService')

class SectionTypePublicController {
  async show({ params, request, response }) {
    const result = await SectionTypePublicService.show(params.idType, request.all())
    return response.status(result.status).json(result)
  }
}

module.exports = SectionTypePublicController

'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const SectionService = use('App/Services/Product/Section/SectionService')

class SectionController {
  async searchSectionByTypes({ params, response }) {
    const result = await SectionService.searchSectionByTypes(params.idType)
    return response.status(result.status).json(result)
  }

  async index({ request, response }) {
    const result = await SectionService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await SectionService.show(params.idSection)
    return response.status(result.status).json(result)
  }

  async store({ request, response }) {
    const data = request.all()

    const rules = {
      name: 'required',
      type_id: 'required',
    }

    const messages = {
      'name.required': 'Um Nome deve ser informado',
      'type_id.required': 'Deve informar o ID do Tipo',
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    const result = await SectionService.store(data)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response }) {
    const result = await SectionService.update(request.all(), params.idSection)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await SectionService.delete(params.idSection)
    return response.status(result.status).json(result)
  }
}

module.exports = SectionController

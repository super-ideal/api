'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const AuxiliaryCodeService = use('App/Services/Product/AuxiliaryCodeService')

class AuxiliaryCodeController {
  async getAll({ params, response }) {
    const result = await AuxiliaryCodeService.getAll(params.product_id)
    return response.status(result.status).json(result)
  }

  async getCode({ params, response }) {
    const result = await AuxiliaryCodeService.getCode(params.code)
    return response.status(result.status).json(result)
  }

  async store({ request, response }) {
    const data = request.all()

    const rules = {
      product_id: 'required',
      box_factor: 'required',
      code: 'required|max:13|unique:product_auxiliary_codes',
    }

    const messages = {
      'product_id.required': 'O ID do produto deve ser informado',
      'code.required': 'O Código Auxiliar deve ser informado',
      'code.max': 'O Código Auxiliar informado é muito grande',
      'code.unique': 'O Código Auxiliar informado já está cadastrado',
      'box_factor.required': 'O fator da embalagem deve ser informado',
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Salva
    const result = await AuxiliaryCodeService.store(data)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response }) {
    const data = request.all()

    const rules = {
      code: 'max:13',
    }

    const messages = {
      'code.max': 'O Código Auxiliar informado é muito grande',
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Atualiza
    const result = await AuxiliaryCodeService.update(data, params.id)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await AuxiliaryCodeService.delete(params.id)
    return response.status(result.status).json(result)
  }
}

module.exports = AuxiliaryCodeController

'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const UnityService = use('App/Services/Product/UnityService')

class UnityController {
  async index({ request, response }) {
    const result = await UnityService.index()
    return response.status(result.status).json(result)
  }
}

module.exports = UnityController

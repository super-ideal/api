'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const ProductService = use('App/Services/Product/ProductService')

class ProductController {
  async index({ request, response }) {
    const result = await ProductService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await ProductService.show(params.idProduct)
    return response.status(result.status).json(result)
  }

  async store({ request, response, auth }) {
    const data = request.all()

    const rules = {
      barcode: 'required|max:50|unique:products',
      description: 'required|max:50',
      price: 'required',
      unity_id: 'required',
    }

    const messages = {
      'barcode.required': 'O Código de Barras deve ser informado',
      'barcode.unique': 'O Código de Barras já está cadastrado',
      'barcode.max': 'O tamanho máximo permitido para o Código de Barras é 50 caracter',
      'description.required': 'Uma Descrição deve ser informada',
      'description.max': 'O tamanho máximo permitido para a Descrição é 50 caracter',
      'price.required': 'O Preço deve ser informado',
      'unity_id.required': 'O ID do Tipo de Unidade deve ser informado',
    }

    const validation = await validateAll(data.product, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Salva
    const result = await ProductService.store(data, auth)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response, auth }) {
    const data = request.all()

    // Atualiza
    const result = await ProductService.update(data, params.idProduct, auth)
    return response.status(result.status).json(result)
  }

  async verifyBarcodeDuplicated({ params, response }) {
    const result = await ProductService.verifyBarcodeDuplicated(params.barcode)
    return response.status(result.status).json(result)
  }

  async productsGroupPrice({ params, request, response }) {
    const result = await ProductService.productsGroupPrice(request.get(), params.idAttributeProduct)
    return response.status(result.status).json(result)
  }
}

module.exports = ProductController

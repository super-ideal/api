'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const OfferService = use('App/Services/Offer/OfferService')

class OfferController {
  async index({ request, response }) {
    const result = await OfferService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await OfferService.show(params.idOffer)
    return response.status(result.status).json(result)
  }

  async store({ request, response, auth }) {
    const data = request.all()

    const rules = {
      name: 'required',
      datetime_start: 'required',
      datetime_end: 'required',
      offer_types_id: 'required'
    }

    const messages = {
      'name.required': 'O Nome da Agenda deve ser informado',
      'datetime_start.required': 'A data de início deve ser informada',
      'datetime_end.required': 'A data final deve ser informada',
      'offer_types_id.required': 'O ID do tipo de oferta deve ser informado'
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    const result = await OfferService.store(data, auth)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response, auth }) {
    const data = request.all()

    const result = await OfferService.update(data, params.idOffer, auth)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await OfferService.delete(params.idOffer)
    return response.status(result.status).json(result)
  }
}

module.exports = OfferController

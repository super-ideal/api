'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const OfferTypeService = use('App/Services/Offer/OfferTypeService')

class OfferTypeController {
  async index({ request, response }) {
    const result = await OfferTypeService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await OfferTypeService.show(params.idType)
    return response.status(result.status).json(result)
  }

  async store({ request, response }) {
    const data = request.all()

    const rules = {
      name: 'required'
    }

    const messages = {
      'name.required': 'Um nome deve ser informado'
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    const result = await OfferTypeService.store(data)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response }) {
    const data = request.all()

    const result = await OfferTypeService.update(data, params.idType)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await OfferTypeService.delete(params.idType)
    return response.status(result.status).json(result)
  }
}

module.exports = OfferTypeController

'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const OfferProduct = use('App/Services/Offer/OfferProductService')

class OfferProductController {
  async productsOffersProgress({ request, response }) {
    const result = await OfferProduct.productsOffersProgress(request.all())
    return response.status(result.status).json(result)
  }

  async verifyProductsInOffer({ request, response }) {
    const result = await OfferProduct.verifyProductsInOffer(request.all())
    return response.status(result.status).json(result)
  }

  async index({ params, request, response }) {
    const result = await OfferProduct.index(request.all(), params.idOffer)
    return response.status(result.status).json(result)
  }

  async show({ params, response }) {
    const result = await OfferProduct.show(params.idOfferProduct)
    return response.status(result.status).json(result)
  }

  async store({ params, request, response, auth }) {
    const data = request.all()

    const rules = {
      product_id: 'required',
    }

    const messages = {
      'product_id.required': 'O ID do produto deve ser informado'
    }

    const validation = await validateAll(data['product'], rules, messages)
    if (validation.fails()) return response.status(400).json({ message: 'Erro na validação', status: 400, data: validation.messages() });

    const result = await OfferProduct.store(data, params.idOffer, auth)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response, auth }) {
    const result = await OfferProduct.update(request.all(), params.idOfferProduct, auth)
    return response.status(result.status).json(result)
  }

  async delete({ params, response }) {
    const result = await OfferProduct.delete(params.idOfferProduct)
    return response.status(result.status).json(result)
  }

  async clear({ params, response }) {
    const result = await OfferProduct.clear(params.idOffer)
    return response.status(result.status).json(result)
  }
}

module.exports = OfferProductController

'use strict'
const OfferProduct = use('App/Services/Offer/Public/OfferProductPublicService')

class OfferProductPublicController {
  
  async productsOffersProgress({ request, response }) {
    const result = await OfferProduct.productsOffersProgress(request.all())
    return response.status(result.status).json(result)
  }
}

module.exports = OfferProductPublicController

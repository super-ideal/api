'use strict'
const OfferProductGroupPrice = use('App/Services/Offer/OfferProductGroupPriceService')

class OfferProductGroupPriceController {
  async index({ params, response }) {
    const result = await OfferProductGroupPrice.index(params.idOffer, params.idOfferProduct)
    return response.status(result.status).json(result)
  }
}

module.exports = OfferProductGroupPriceController

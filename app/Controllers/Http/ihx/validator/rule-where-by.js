'use strict';

exports.ruleWhereBy = (res) => {
  const Acesso = require('../../../../Models/ihx/Acesso')

  var rules = Acesso.visible;

  if (rules.indexOf(res) > -1)
    return true;

  return false;
}

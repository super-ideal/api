'use strict'
const ErrorException = use('App/Exceptions/ErrorException')
const { validateAll } = use('Validator')

const ShoppingListProductsService = use('App/Services/Public/ShoppingLists/ShoppingListProductsService')

class ShoppingListProductsController {
    async getAllPublic({ request, params, response }) {
    const result = await ShoppingListProductsService.getAllPublic(params.idList, request.all())
    return response.status(result.status).json(result)
  }

  async index({ request, params, response, auth }) {
    const result = await ShoppingListProductsService.index(params.idList, request.all(), auth)
    return response.status(result.status).json(result)
  }

  async store({ request, response, auth }) {
    const data = request.all()

    const rules = {
      product_id: 'required',
      list_id: 'required'
    }

    const messages = {
      'product_id.required': 'O ID do produto deve ser informado',
      'list_id.required': 'O ID da lista de compra deve ser informado'
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ErrorException(validation.messages(), 422)

    const result = await ShoppingListProductsService.store(data, auth)
    return response.status(result.status).json(result)
  }

  async update({ params, request, response, auth }) {
    const data = request.all()

    const result = await ShoppingListProductsService.update(data, params.idList, params.idListProduct, auth)
    return response.status(result.status).json(result)
  }

  async delete({ params, response, auth }) {
    const result = await ShoppingListProductsService.delete(params.idList, params.idListProduct, auth)
    return response.status(result.status).json(result)
  }
}

module.exports = ShoppingListProductsController

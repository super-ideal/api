'use strict'
const UserService = use('App/Services/User/UserService')
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

// https://adonisjs.com/docs/4.0/relationships#_querying_data
class UserController {
  async index({ request, response }) {
    const result = await UserService.index(request.all())
    return response.status(result.status).json(result)
  }

  async show ({params, response}) {
    const result = await UserService.showUser(params.id)
    return response.status(result.status).json(result)
  }

  async permissionMany ({params, response}) {
    const result = await UserService.permissionMany(params.id)
    return response.status(result.status).json(result)
  }

  async permissionOne ({params, response}) {
    const result = await UserService.permissionOne(params.id, params.page, params.action)
    return response.status(result.status).json(result)
  }

  async store ({request, response}) {
    const userData = request.all()

    // Validation
    const rules = {
      username: 'required|unique:users|max:50',
      email: 'required|email|unique:users,email',
      password: 'required|confirmed|min:6|max:30',
      partner_id: 'required|unique:users'
    }

    const messages = {
      'username.required': 'É necessário informar um nome de usuário',
      'username.unique': 'O nome de usuário informado já está cadastrado',
      'username.max': 'O tamanho máximo do nome de usuário é de 50 caracteres',
      'email.required': 'O email é necessário para uma maior comunicação',
      'email.unique': 'O email informado já está cadastrado',
      'email.email': 'O email informado não é válido',
      'password.min': 'O tamanho mínimo da senha é de 6 caracteres',
      'password.max': 'O tamanho máximo da senha é de 30 caracteres',
      'password.required': 'Uma senha deve ser informada',
      'password.confirmed': 'A senha de confirmação não é válida',
      'partner_id.required': 'Você deve fornecer o ID da pessoa relacionada ao usuário',
      'partner_id.unique': 'Já existe um usuário com o parceiro informado',
    }

    const validation = await validateAll(userData, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Save
    delete userData['password_confirmation']

    const result = await UserService.storeUser(userData)
    return response.status(result.status).json(result)
  }

  async update ({params, request, response}) {
    const userData = request.all()

    // Validation
    const rules = {
      username: 'max:50',
      email: 'email',
      password: 'confirmed|min:6|max:30',
    }

    const messages = {
      'username.max': 'O tamanho máximo do nome de usuário é de 50 caracteres',
      'email.email': 'O email informado não é válido',
      'password.min': 'O tamanho mínimo da senha é de 6 caracteres',
      'password.max': 'O tamanho máximo da senha é de 30 caracteres',
      'password.confirmed': 'A senha de confirmação não é válida',
    }

    const validation = await validateAll(userData, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Save
    delete userData['password_confirmation']

    const result = await UserService.updateUser(userData, params.id)
    return response.status(result.status).json(result)
  }
}

module.exports = UserController

'use strict'
const ValidationException = use('App/Exceptions/ValidationException')
const { validateAll } = use('Validator')

const ActionService = use('App/Services/Acl/ActionService')

class ActionAclController {
  async storeAction ({ request, response }) {
    const data = request.all()

    const rules = {
      name: 'required|max:80|unique:acl_actions',
      description: 'max:255',
    }

    const messages = {
      'name.required': 'Você deve informar o nome da Ação',
      'name.max': 'A Ação informada é muito grande',
      'name.unique': 'A Ação informada já está cadastrada',
      'description.max': 'A Ação informada é muito grande',
    }

    const validation = await validateAll(data, rules, messages)
    if (validation.fails()) throw new ValidationException(validation.messages())

    // Salva
    const result = await ActionService.store(data)
    return response.status(result.status).json(result)
  }

  async indexAction ({ response }) {
    const result = await ActionService.index()
    return response.status(result.status).json(result)
  }

  async deleteAction({ params, response }) {
    const result = await ActionService.deleteAction(params.id)
    return response.status(result.status).json(result)
  }
}

module.exports = ActionAclController

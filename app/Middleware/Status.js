'use strict'

const ErrorException = use('App/Exceptions/ErrorException')

class Status {

  async handle ({ request, response, auth }, next, schemes) {
    try {
      const user = auth.current.user
      const status = schemes
      let isHasStatus = false

      if (user.status.toLowerCase() === 'admin') {
        isHasStatus = true
      }

      for (const s of status) {
        if (user.status.toLowerCase() === s.toLowerCase()) {
          isHasStatus = true
          break
        }
      }

      if (!isHasStatus)
        throw new ErrorException("Você não tem permissão para prosseguir com a solitação", 401)

      // call next to advance the request
      await next()

    } catch(error) {
      throw new ErrorException(error.message, error.status, error.code)
    }
  }

}

module.exports = Status

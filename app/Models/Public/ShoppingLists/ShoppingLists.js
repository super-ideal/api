'use strict'

const Model = use('Model')

class ShoppingLists extends Model {
  static get table() {
    return 'shopping_lists'
  }

  user() {
    return this.belongsTo('App/Models/User/User')
  }

  listProducts() {
    return this.hasMany('App/Models/Public/ShoppingLists/ShoppingListProducts', 'id', 'list_id')
  }
}

module.exports = ShoppingLists

'use strict'

const Model = use('Model')

class ShoppingListsView extends Model {
  static get table() {
    return 'shopping_lists_view'
  }

  user() {
    return this.belongsTo('App/Models/User/User')
  }

  listProducts() {
    return this.hasMany('App/Models/Public/ShoppingLists/ShoppingListProducts', 'id', 'list_id')
  }
}

module.exports = ShoppingListsView

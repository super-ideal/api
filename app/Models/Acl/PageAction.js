'use strict'

const Model = use('Model')

class PageAction extends Model {
  static get table() {
    return 'acl_page_actions'
  }

  static get createdAtColumn() {
    return null
  }

  static get updatedAtColumn() {
    return null
  }

  pages() {
    return this.belongsTo('App/Models/Acl/Page')
  }

  pageActionUser() {
    return this.hasMany('App/Models/Acl/PageActionUser')
  }
}

module.exports = PageAction

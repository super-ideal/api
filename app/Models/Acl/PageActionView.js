'use strict'

const Model = use('Model')

class PageActionView extends Model {
  static get table () {
    return 'acl_page_action_view'
  }

  pages() {
    return this.belongsTo('App/Models/Acl/Page')
  }

  pageActionUser() {
    return this.hasMany('App/Models/Acl/PageActionUser')
  }
}

module.exports = PageActionView

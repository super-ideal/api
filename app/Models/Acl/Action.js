'use strict'

const Model = use('Model')

class Action extends Model {
  static get table () {
    return 'acl_actions'
  }

  pages() {
    return this
      .belongsToMany('App/Models/Acl/Page')
      .pivotModel('App/Models/Acl/PageAction')
  }
}

module.exports = Action

'use strict'

const Model = use('Model')

// PivotModel
class PageActionUserView extends Model {
  static get table () {
    return 'acl_page_action_users_view'
  }
}

module.exports = PageActionUserView

'use strict'

const Model = use('Model')

class Image extends Model {
  static get table () {
    return 'product_images'
  }

  // Product -> IMAGE
  product () {
    return this.belongsToMany('App/Models/Product/Product')
  }
}

module.exports = Image

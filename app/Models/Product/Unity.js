'use strict'

const Model = use('Model')

class Unity extends Model {
  static get table () {
    return 'product_unitys'
  }

  static get createdAtColumn () {
    return null
  }

  static get updatedAtColumn () {
    return null
  }

  // UNITY -> Product
  product () {
    return this.hasOne('App/Models/Product/Product')
  }
}

module.exports = Unity

'use strict'

const Model = use('Model')

class AuxiliaryCode extends Model {
  static get table () {
    return 'product_auxiliary_codes'
  }

  // Product -> AuxiliaryCode
  product () {
    return this.belongsTo('App/Models/Product/Product')
  }
}

module.exports = AuxiliaryCode

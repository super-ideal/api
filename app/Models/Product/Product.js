'use strict'

const Model = use('Model')

class Product extends Model {
  getActive (active) {
    return active === 1 ? true : false
  }

  setActive (active) {
    return (active == true || active == 'true' || active == 1) ? 1 : 0
  }

  // Unity -> PRODUCT
  unity () {
    return this.belongsTo('App/Models/Product/Unity')
  }

  // Product -> Weight
  weight () {
    return this.hasMany('App/Models/Product/Weight')
  }

  // Product -> AuxiliaryCode
  auxiliaryCode () {
    return this.hasMany('App/Models/Product/AuxiliaryCode')
  }

  // http://adonisjs.com/docs/4.0/relationships#_pivotmodel
  // Product -> AttributesProduct
  attributesProduct () {
    return this
      .belongsToMany('App/Models/Product/Attributes/Attributes')
      .pivotModel('App/Models/Product/Attributes/AttributesProduct')
  }

  // Product -> AttributesProduct
  sectionProduct () {
    return this
      .belongsToMany('App/Models/Product/Section/Section')
      .pivotModel('App/Models/Product/Section/SectionProduct')
  }

  // Image -> PRODUCT
  images () {
    return this.hasMany('App/Models/Product/Image')
  }

  // User -> PRODUCT
  created () {
    return this.belongsTo('App/Models/User/User', 'created_by')
  }

  // User -> PRODUCT
  updated () {
    return this.belongsTo('App/Models/User/User', 'updated_by')
  }

  // OfferProduct -> PRODUCT
  offerProduct () {
    return this.hasMany('App/Models/Offer/OfferProduct')
  }

  // OfferProduct -> PRODUCT
  productAuxiliaryCodes () {
    return this.hasMany('App/Models/Product/AuxiliaryCode')
  }
}

module.exports = Product

'use strict'

const Model = use('Model')

class Weight extends Model {
  static get table () {
    return 'product_weights'
  }

  // Product -> Weight
  product () {
    return this.hasOne('App/Models/Product/Product')
  }
}

module.exports = Weight

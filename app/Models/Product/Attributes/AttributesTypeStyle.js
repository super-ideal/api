'use strict'

const Model = use('Model')

class TypeStyle extends Model {
  static get table () {
    return 'product_attributes_types_styles'
  }

  static get createdAtColumn() {
    return null
  }

  static get updatedAtColumn() {
    return null
  }

  static get incrementing() {
    return false
  }

  type () {
    return this.hasOne('App/Models/Product/Attributes/AttributesType')
  }
}

module.exports = TypeStyle

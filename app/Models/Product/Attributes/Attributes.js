'use strict'

const Model = use('Model')

class Attributes extends Model {
  static get table () {
    return 'product_attributes'
  }

  getActive(active) {
    return active == 1 ? true : false
  }

  setActive(active) {
    return (active == true || active == 'true' || active == 1) ? 1 : 0
  }

  // AttributesType -> Attributes
  type () {
    return this.belongsTo('App/Models/Product/Attributes/AttributesType')
  }
}

module.exports = Attributes

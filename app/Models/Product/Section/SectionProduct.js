'use strict'

const Model = use('Model')

class SectionProduct extends Model {
  static get createdAtColumn () {
    return null
  }

  static get updatedAtColumn () {
    return null
  }

  static get table () {
    return 'product_section_products'
  }
}

module.exports = SectionProduct

'use strict'

const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     *
     * Look at `app/Models/Hooks/User.js` file to
     * check the hashPassword method
     */
    this.addHook('beforeCreate', 'User.hashPassword')
    //this.addHook('beforeUpdate', 'User.hashPassword')
    //this.addHook('beforeSave', 'User.hashPassword')
  }

  static get hidden () {
    return ['password']
  }

  getActive (active) {
    return active === 1 ? true : false
  }

  setActive (active) {
    return (active == true || active == 'true' || active == 1) ? 1 : 0
  }

   /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/User/Token')
  }

  partner () {
    return this.belongsTo('App/Models/Partner/Partner')
  }

  // http://adonisjs.com/docs/4.0/relationships#_pivotmodel
  pageActionUser () {
    return this
      .belongsToMany('App/Models/Acl/PageAction')
      .pivotModel('App/Models/Acl/PageActionUser')
  }

  authSocial () {
    return this.hasMany('App/Models/User/AuthSocial')
  }
}

module.exports = User

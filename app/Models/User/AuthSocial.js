'use strict'

const Model = use('Model')

class AuthSocial extends Model {
  // User -> AuthSocial
  user () {
    return this.belongsTo('App/Models/User/User')
  }
}

module.exports = AuthSocial

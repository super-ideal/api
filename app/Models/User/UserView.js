'use strict'

const Model = use('Model')

class UserView extends Model {
	static get table () {
		return 'users_view'
  }

  static get hidden () {
    return ['password']
  }

  tokens () {
    return this.hasMany('App/Models/User/Token', 'id', 'user_id')
  }
  
  permissions() {
    return this.hasMany('App/Models/Acl/PageActionUserView', 'id', 'user_id')
  }

  // User -> AuthSocial
  authSocial () {
    return this.hasMany('App/Models/User/AuthSocial')
  }

  // Partner -> USER
  partner () {
    return this.belongsTo('App/Models/Partner/Partner')
  }
}

module.exports = UserView

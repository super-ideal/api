'use strict'

const Model = use('Model')

class Partner extends Model {
  static get table () {
    return 'partners'
  }

  getActive(active) {
    return active == 1 ? true : false
  }

  setActive(active) {
    return (active == true || active == 'true' || active == 1) ? 1 : 0
  }

  user() {
    return this.hasOne('App/Models/User/User')
  }

  adresses() {
    return this.hasMany('App/Models/Partner/PartnerAddress')
  }

  phones() {
    return this.hasMany('App/Models/Partner/PartnerPhone')
  }

  emails() {
    return this.hasMany('App/Models/Partner/PartnerEmail')
  }

  added () {
    return this.belongsTo('App/Models/User/User', 'added_by')
  }

  updated () {
    return this.belongsTo('App/Models/User/User', 'updated_by')
  }

  // http://adonisjs.com/docs/4.0/relationships#_pivotmodel
  // Partner -> PartnerType
  partnerType () {
    return this
      .belongsToMany('App/Models/Partner/PartnerType')
      .pivotModel('App/Models/Partner/PartnerTypePartner')
  }
}

module.exports = Partner

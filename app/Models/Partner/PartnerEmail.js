'use strict'

const Model = use('Model')

class PartnerEmail extends Model {
  static get table () {
    return 'partner_emails'
  }

  // User -> OFFERPRODUCT
  added () {
    return this.belongsTo('App/Models/User/User', 'added_by')
  }

  // User -> OFFERPRODUCT
  updated () {
    return this.belongsTo('App/Models/User/User', 'updated_by')
  }

  partner () {
    return this.belongsTo('App/Models/Partner/Partner')
  }
}

module.exports = PartnerEmail

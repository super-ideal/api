'use strict'

const Model = use('Model')

class PartnerType extends Model {
  static get table () {
    return 'partner_types'
  }

  static get createdAtColumn() {
    return null
  }

  static get updatedAtColumn() {
    return null
  }
}

module.exports = PartnerType

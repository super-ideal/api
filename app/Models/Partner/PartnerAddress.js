'use strict'

const Model = use('Model')

class PartnerAddress extends Model {
  static get table () {
    return 'partner_adresses'
  }

  // User -> OFFERPRODUCT
  added () {
    return this.belongsTo('App/Models/User/User', 'added_by')
  }

  // User -> OFFERPRODUCT
  updated () {
    return this.belongsTo('App/Models/User/User', 'updated_by')
  }

  partner () {
    return this.belongsTo('App/Models/Partner/Partner')
  }
}

module.exports = PartnerAddress

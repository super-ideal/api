'use strict'

const Model = use('Model')

class OfferProductsGroupPrice extends Model {
  static get table () {
    return 'offer_products_group_price'
  }

  static get createdAtColumn () {
    return null
  }

  static get updatedAtColumn () {
    return null
  }

  product () {
    return this.belongsToMany('App/Models/Product/Product')
  }

  offerProduct () {
    return this.belongsToMany('App/Models/Offer/OfferProduct')
  }
}

module.exports = OfferProductsGroupPrice

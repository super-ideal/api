'use strict'

const Model = use('Model')

class OfferType extends Model {
  static get table () {
    return 'offer_types'
  }

  getActive (active) {
    return active === 1 ? true : false
  }

  setActive (active) {
    return (active == true || active == 'true' || active == 1) ? 1 : 0
  }

  getEditable (editable) {
    return editable === 1 ? true : false
  }

  setEditable (editable) {
    return (editable == true || editable == 'true' || editable == 1) ? 1 : 0
  }

  // Offer -> OFFERTYPE
  offer () {
    return this.hasOne('App/Models/Offer/Offer')
  }
}

module.exports = OfferType

'use strict'

const Model = use('Model')

class OfferProductProgress extends Model {
  static get table () {
    return 'offer_products_progress_view'
  }

  images() {
    return this.hasMany('App/Models/Product/Image', 'product_id', 'product_id')
  }

  unity_product() {
    return this.hasOne('App/Models/Product/Unity', 'unity_id', 'id')
  }
}

module.exports = OfferProductProgress

'use strict'

const Model = use('Model')

class OfferMotive extends Model {
  static get table () {
    return 'offer_products_motives'
  }

  getActive (active) {
    return active === 1 ? true : false
  }

  setActive (active) {
    return (active == true || active == 'true' || active == 1) ? 1 : 0
  }

  getEditable (editable) {
    return editable === 1 ? true : false
  }

  setEditable (editable) {
    return (editable == true || editable == 'true' || editable == 1) ? 1 : 0
  }

  // OfferProduct -> OFFERMOTIVE
  offerProduct () {
    return this.hasOne('App/Models/Offer/OfferProduct')
  }
}

module.exports = OfferMotive

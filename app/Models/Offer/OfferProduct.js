'use strict'

const Model = use('Model')

class OfferProduct extends Model {
  static get table () {
    return 'offer_products'
  }

  getActive (active) {
    return active === 1 ? true : false
  }

  setActive (active) {
    return (active == true || active == 'true' || active == 1) ? 1 : 0
  }

  getTopOffer(top_offer) {
    return top_offer === 1 ? true : false
  }

  setTopOffer (top_offer) {
    return (top_offer == true || top_offer == 'true' || top_offer == 1) ? 1 : 0
  }

  getTypeDiscountPercentage (type_discount_percentage) {
    return type_discount_percentage === 1 ? true : false
  }

  setTypeDiscountPercentage (type_discount_percentage) {
    return (type_discount_percentage == true || type_discount_percentage == 'true' || type_discount_percentage == 1) ? 1 : 0
  }

  getTypeDiscountMoney (type_discount_money) {
    return type_discount_money === 1 ? true : false
  }

  setTypeDiscountMoney (type_discount_money) {
    return (type_discount_money == true || type_discount_money == 'true' || type_discount_money == 1) ? 1 : 0
  }

  getPriceGroup (price_group) {
    return price_group === 1 ? true : false
  }

  setPriceGroup (price_group) {
    return (price_group == true || price_group == 'true' || price_group == 1) ? 1 : 0
  }

  getViewPercentDiscount (view_percent_discount) {
    return view_percent_discount === 1 ? true : false
  }

  setViewPercentDiscount (view_percent_discount) {
    return (view_percent_discount == true || view_percent_discount == 'true' || view_percent_discount == 1) ? 1 : 0
  }

  // Offer -> OFFERPRODUCT
  offer () {
    return this.belongsTo('App/Models/Offer/OfferProduct')
  }

  // Product -> OFFERPRODUCT
  product () {
    return this.belongsTo('App/Models/Product/Product')
  }

  // OfferMotive -> OFFERPRODUCT
  offerMotive () {
    return this.belongsTo('App/Models/Offer/OfferMotive')
  }

  // User -> OFFERPRODUCT
  added () {
    return this.belongsTo('App/Models/User/User', 'added_by')
  }

  // User -> OFFERPRODUCT
  updated () {
    return this.belongsTo('App/Models/User/User', 'updated_by')
  }

  productPriceGroup () {
    return this.hasMany('App/Models/Offer/OfferProductsGroupPrice')
  }
}

module.exports = OfferProduct

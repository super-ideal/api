# Adonis API application

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick --api-only
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.
```js
adonis migration:run
adonis migration:reset
adonis migration:rollback
```

```js
adonis seed
```

### Publicar API para produção
Setar nas variaveis de ambiente:
- DB_HOST
- DB_PORT
- DB_USER
- DB_PASSWORD
- DB_DATABASE
- DB_CONNECTION
- APP_KEY
- ENV_SILENT = true

### Heroku
- heroku maintenance:on (Coloca o servidor em manutanção)
- heroku ps:scale web=1 (Certifica de que pelo menos uma instância do aplicativo esteja em execução)
- heroku open (Abre o aplicativo)
- heroku logs --tail (Ver logs do aplicativo em execução)
- heroku ps (Verificar quantos dynos estão executando)
- heroku local web (Inicia o aplicativo localmente para testes)
- heroku restart -a app_name (reinicia a aplicação)
- heroku ps:restart [web|worker] --app app_name (Reinicia um determinado processo Ex.: heroku ps:restart web.2 --app app_name)
- heroku run bash (Acessar o console da aplicação)
- git push heroku master (Envia novo push para a aplicação)

- heroku config (Visualiza as configVars definidas na aplicação)
- heroku config:get GITHUB_USERNAME (Visualiza uma determinada configVars)
- heroku config:set GITHUB_USERNAME=joesmith (Adiciona uma nova configVars)
- heroku config:unset GITHUB_USERNAME (Remove uma configVars)

- heroku addons:open papertrail (Visualiza os logs da aplicação: addons papertrail)

- https://devcenter.heroku.com/articles/getting-started-with-nodejs#push-local-changes
- https://scotch.io/tutorials/deploying-adonisjs-apps-to-heroku

### Links Úteis

- [Validation](http://indicative.adonisjs.com/#indicative-basics-custom-messages)
- [Migations - Columns](http://knexjs.org/#Schema-Building)
- [Adonis4 Cloudinary](https://github.com/kingsinbad/adonis-4-cloudinary)

'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

const Factory = use('Factory')

/** Autenticação */
// Tipo Parceiro
Factory.blueprint('App/Models/Partner/PartnerType', async (faker, i, data) => {
  return {
    name: data.name
  }
})

// Parceiro
/*Factory.blueprint('App/Models/Partner/Partner', async (faker) => {
  return {
    name_reason: faker.name(),
    type_person: 'Fisíca',
  }
})*/

// Usuário
Factory.blueprint('App/Models/User/User', async (faker, i, data) => {
  return {
    username: 'duducp', //faker.username(),
    email: 'duducp2013@gmail.com', //faker.email(),
    password: '123456', //await Hash.make(faker.password()),
    status: data.status
  }
})

/** Produtos */
// Unidade Medida
Factory.blueprint('App/Models/Product/Unity', async (faker, i, data) => {
  return {
    name: data.data.name,
    abbreviation: data.data.abbreviation
  }
})

// Tipo de Seção
Factory.blueprint('App/Models/Product/Section/SectionType', async (faker, i, data) => {
  return {
    editable: data.data.editable,
    required: data.data.required,
    name: data.data.name,
    id_unique: data.data.id_unique,
    parent_id: data.data.parent_id
  }
})

// Seçãos
Factory.blueprint('App/Models/Product/Section/Section', async (faker, i, data) => {
  return {
    active: data.data.active,
    name: data.data.name,
    parent_id: data.data.parent_id,
    description: data.data.description,
    type_id: data.data.type_id,
  }
})

// Attribute Type Style
Factory.blueprint('App/Models/Product/Attributes/AttributesTypeStyle', async (faker, i, data) => {
  return {
    id: data.data.id,
    name: data.data.name,
    description: data.data.description
  }
})

// Attribute Type
Factory.blueprint('App/Models/Product/Attributes/AttributesType', async (faker, i, data) => {
  return {
    active: data.data.active,
    id_unique: data.data.id_unique,
    style_id: data.data.style_id,
    required: data.data.required,
    editable: data.data.editable,
    public: data.data.public,
    name: data.data.name,
    description: data.data.description
  }
})

// Offer Type
Factory.blueprint('App/Models/Offer/OfferType', async (faker, i, data) => {
  return {
    id_unique: data.data.id_unique,
    editable: data.data.editable,
    name: data.data.name,
    active: data.data.active,
    opt_top_offer: data.data.opt_top_offer,
    opt_discount: data.data.opt_discount,
    opt_discount_price_two: data.data.opt_discount_price_two,
    opt_cumulative: data.data.opt_cumulative,
    opt_cumulative_payable: data.data.opt_cumulative_payable,
    opt_wholesale: data.data.opt_wholesale,
    opt_price_group: data.data.opt_price_group,
    opt_limit_per_coupon: data.data.opt_limit_per_coupon,
    opt_view_percent_discount: data.data.opt_view_percent_discount
  }
})

// Offer Product Motives
Factory.blueprint('App/Models/Offer/OfferMotive', async (faker, i, data) => {
  return {
    active: data.data.active,
    editable: data.data.editable,
    name: data.data.name
  }
})

/** ACL */
Factory.blueprint('App/Models/Acl/Page', async (faker, i, data) => {
  return {
    name: data.data.name,
    description: data.data.description,
    tree: data.data.tree,
    level: data.data.level
  }
})

Factory.blueprint('App/Models/Acl/Action', async (faker, i, data) => {
  return {
    name: data.data.name,
    description: data.data.description
  }
})

Factory.blueprint('App/Models/Acl/PageAction', async (faker, i, data) => {
  return {
    page_id: data.data.page_id,
    action_id: data.data.action_id
  }
})

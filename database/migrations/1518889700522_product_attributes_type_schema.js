'use strict'

const Schema = use('Schema')

class ProductAttributesTypeSchema extends Schema {
  up () {
    this.create('product_attributes_types', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.string('id_unique', 30).nullable().unique().comment('Campo interno, não pode ser apresentado no front-end')
      table.boolean('editable').default(true).notNullable().comment('Campo interno, não pode ser apresentado no front-end')
      table.boolean('public').default(true).notNullable().comment('Se falso, o atribute será visível apenas no painel de controle e relatórios')
      table.boolean('active').default(true).notNullable()
      table.boolean('required').default(false).notNullable()
      table.string('style_id', 50).references('id').inTable('product_attributes_types_styles').notNullable()
      table.string('name', 80).notNullable().unique()
      table.string('description')
      table.timestamps()
    })
  }

  down () {
    this.drop('product_attributes_types')
  }
}

module.exports = ProductAttributesTypeSchema

'use strict'

const Schema = use('Schema')

class ProductUnitysSchema extends Schema {
  up () {
    this.create('product_unitys', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.string('name', 15).notNullable().unique()
      table.string('abbreviation', 4).notNullable().unique()
    })
  }

  down () {
    this.drop('product_unitys')
  }
}

module.exports = ProductUnitysSchema

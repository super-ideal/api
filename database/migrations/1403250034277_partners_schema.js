'use strict'

const Schema = use('Schema')

class PartnersSchema extends Schema {
  static get connection () {
    return 'mysql'
  }

  up () {
    this.create('partners', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.boolean('active').defaultTo(true)
      table.string('name_reason').notNullable()
      table.string('name_fantasy')
      table.bigInteger('cpf_cnpj', 14).unique()
      table.string('rg_issuing_body', 6)
      table.string('rg_uf', 2)
      table.bigInteger('rg_ie', 14).unique()
      table.string('type_person', 20).notNullable()
      table.string('sex', 10)
      table.integer('added_by').unsigned()//.references('id').inTable('users')
      table.integer('updated_by').unsigned()//.references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('partners')
  }
}

module.exports = PartnersSchema

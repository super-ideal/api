'use strict'

const Schema = use('Schema')

class OfferProductsGroupPriceSchema extends Schema {
  up () {
    this.create('offer_products_group_price', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('offer_id').unsigned().references('id').inTable('offers').notNullable()
      table.integer('offer_product_id').unsigned().references('id').inTable('offer_products').notNullable()
      table.integer('product_id').unsigned().references('id').inTable('products').notNullable()
      table.unique(['product_id', 'offer_id'])
    })
  }

  down () {
    this.drop('offer_products_group_price')
  }
}

module.exports = OfferProductsGroupPriceSchema

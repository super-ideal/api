'use strict'

const Schema = use('Schema')

class OfferProductsSchema extends Schema {
  up () {
    this.create('offer_products', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('product_id').unsigned().references('id').inTable('products').notNullable()
      table.integer('offer_id').unsigned().references('id').inTable('offers').notNullable()
      table.integer('offer_motive_id').unsigned().references('id').inTable('offer_products_motives')
      table.boolean('active').default(true).notNullable().comment('Informa se o produto está ativo na agenda de oferta')
      table.boolean('top_offer').default(false).notNullable()
      table.boolean('type_discount_percentage').default(false).comment('Aqui é definido se o tipo de desconto a ser realizado é por PORCENTAGEM')
      table.boolean('type_discount_money').default(false).comment('Aqui é definido se o tipo de desconto a ser realizado é em DINHEIRO')
      table.decimal('discount').unsigned().comment('Esse é o valor do desconto, em cima do preço do produto, de acordo com o Tipo de Desconto')
      table.decimal('discount_price_two').unsigned().comment('Esse é o valor do desconto para o preço dois. Ele se baseia em cima do valor do produto menos o desconto já aplicado.')
      table.integer('cumulative').unsigned().comment('Caso a promoção seja acumulativa, aqui é informado a quantidade de itens para se obter o desconto')
      table.integer('cumulative_payable').unsigned().comment('Caso a promoção seja do tipo leve 2 pague 1, esse campo serve para especificar a quantidade a ser paga pelo cliente')
      table.boolean('wholesale').default(false).notNullable().comment('Informa se o produto esta em oferta para atacado.')
      table.boolean('price_group').default(false).notNullable().comment('Informa se é para aplicar o desconto no grupo de preço do produto')
      table.boolean('view_percent_discount').default(false).notNullable().comment('Informa se é para apresentar a porcentagem de desconto que está tendo no produto')
      table.decimal('limit_per_coupon').unsigned().comment('Informa a quantidade máxima de produto por cupom para obter o desconto')
      table.integer('added_by').unsigned().references('id').inTable('users').notNullable()
      table.integer('updated_by').unsigned().references('id').inTable('users')
      table.unique(['product_id', 'offer_id'])
      table.timestamps()
    })
  }

  down () {
    this.drop('offer_products')
  }
}

module.exports = OfferProductsSchema

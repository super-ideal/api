'use strict'

const Schema = use('Schema')

class PartnerAdressesSchema extends Schema {
  up () {
    this.create('partner_adresses', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('cep', 8).notNullable()
      table.string('street', 50).notNullable()
      table.string('number', 10)
      table.string('complement', 50)
      table.string('neighborhood', 50).notNullable()
      table.string('city', 50).notNullable()
      table.string('state', 50).notNullable()
      table.integer('partner_id').unsigned().references('id').inTable('partners').notNullable()
      table.integer('added_by').unsigned().references('id').inTable('users').notNullable()
      table.integer('updated_by').unsigned().references('id').inTable('users')
      table.unique(['street', 'number', 'cep'])
      table.timestamps()
    })
  }

  down () {
    this.drop('partner_adresses')
  }
}

module.exports = PartnerAdressesSchema

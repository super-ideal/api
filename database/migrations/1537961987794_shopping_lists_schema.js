'use strict'

const Schema = use('Schema')

class ShoppingListsSchema extends Schema {
  up () {
    this.create('shopping_lists', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.string('name', 50).notNullable()
      table.string('note')
      table.timestamp('date_for_purchase').default(null).nullable().comment('Informa a data para compra dos produtos')
      table.boolean('public').default(false).notNullable().comment('Informa se a lista de compra é publica')
      table.boolean('shared').default(false).notNullable().comment('Informa se a lista de compra está sendo compartilhada')
      table.integer('user_id').unsigned().references('id').inTable('users').notNullable()
      // table.integer('category_id').unsigned().references('id').inTable('shopping_list_categories').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('shopping_lists')
  }
}

module.exports = ShoppingListsSchema

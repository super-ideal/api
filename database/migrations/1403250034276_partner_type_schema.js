'use strict'

const Schema = use('Schema')

class PartnerTypePartnerSchema extends Schema {
  up () {
    this.create('partner_types', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.string('name').notNullable().unique()
    })
  }

  down () {
    this.drop('partner_types')
  }
}

module.exports = PartnerTypePartnerSchema

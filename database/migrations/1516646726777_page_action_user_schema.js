'use strict'

const Schema = use('Schema')

class PageActionUserSchema extends Schema {
  up () {
    this.create('acl_page_action_users', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users').notNullable()
      table.integer('page_action_id').unsigned().references('id').inTable('acl_page_actions').notNullable()
      table.unique(['user_id', 'page_action_id'])
    })
  }

  down () {
    this.drop('acl_page_action_users')
  }
}

module.exports = PageActionUserSchema

'use strict'

const Schema = use('Schema')

class ProductAttributesProductSchema extends Schema {
  up () {
    this.create('product_attributes_products', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('product_id').unsigned().references('id').inTable('products').notNullable()
      table.integer('attribute_id').unsigned().references('id').inTable('product_attributes').notNullable()
      table.unique(['product_id', 'attribute_id'])
    })
  }

  down () {
    this.drop('product_attributes_products')
  }
}

module.exports = ProductAttributesProductSchema

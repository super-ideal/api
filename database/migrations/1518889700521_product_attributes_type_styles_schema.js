'use strict'

const Schema = use('Schema')

class ProductAttributesTypeStylesSchema extends Schema {
  up () {
    this.create('product_attributes_types_styles', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.string('id', 50).primary().unique().notNullable()
      table.string('name', 80).notNullable().unique()
      table.string('description')
    })
  }

  down () {
    this.drop('product_attributes_types_styles')
  }
}

module.exports = ProductAttributesTypeStylesSchema

'use strict'

const Schema = use('Schema')

class ProductSectionTypeSchema extends Schema {
  up () {
    this.create('product_section_types', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('parent_id').unsigned().references('id').inTable('product_section_types')
      table.string('id_unique', 30).nullable().unique()
      table.boolean('required').default(true).notNullable()
      table.boolean('editable').default(true).notNullable()
      table.string('name', 80).notNullable().unique()
      table.timestamps()
    })
  }

  down () {
    this.drop('product_section_types')
  }
}

module.exports = ProductSectionTypeSchema

'use strict'

const Schema = use('Schema')

class OfferTypeSchema extends Schema {
  up () {
    this.create('offer_types', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.boolean('active').default(true).notNullable()
      table.string('name', 50).notNullable().unique()
      table.string('description')
      table.string('id_unique', 30).nullable().unique()
      table.boolean('editable').default(true).notNullable()
      table.boolean('opt_top_offer').default(false).notNullable()
      table.boolean('opt_discount').default(false).notNullable()
      table.boolean('opt_discount_price_two').default(false).notNullable()
      table.boolean('opt_cumulative').default(false).notNullable()
      table.boolean('opt_cumulative_payable').default(false).notNullable()
      table.boolean('opt_wholesale').default(false).notNullable()
      table.boolean('opt_price_group').default(false).notNullable()
      table.boolean('opt_limit_per_coupon').default(false).notNullable()
      table.boolean('opt_view_percent_discount').default(false).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('offer_types')
  }
}

module.exports = OfferTypeSchema

'use strict'

const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.boolean('active').default(true).notNullable()
      table.string('barcode').notNullable().unique()
      table.string('description').notNullable()
      table.text('description_long')
      table.decimal('price').unsigned().notNullable()
      table.integer('unity_id').unsigned().references('id').inTable('product_unitys').notNullable()
      table.integer('created_by').unsigned().references('id').inTable('users').notNullable()
      table.integer('updated_by').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema

'use strict'

const Schema = use('Schema')

class OfferMotiveSchema extends Schema {
  up () {
    this.create('offer_products_motives', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.boolean('active').default(true).notNullable()
      table.string('name', 50).notNullable()
      table.string('description')
      table.boolean('editable').default(true).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('offer_products_motives')
  }
}

module.exports = OfferMotiveSchema

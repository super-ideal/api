'use strict'

const Schema = use('Schema')

class PartnerEmailsSchema extends Schema {
  up () {
    this.create('partner_emails', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.string('email').notNullable().unique()
      table.string('description')
      table.string('type', 50).notNullable()
      table.integer('partner_id').unsigned().references('id').inTable('partners').notNullable()
      table.integer('added_by').unsigned().references('id').inTable('users').notNullable()
      table.integer('updated_by').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('partner_emails')
  }
}

module.exports = PartnerEmailsSchema

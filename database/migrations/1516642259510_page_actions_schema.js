'use strict'

const Schema = use('Schema')

class PageActionsSchema extends Schema {
  up () {
    this.create('acl_page_actions', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('page_id').unsigned().references('id').inTable('acl_pages').notNullable()
      table.integer('action_id').unsigned().references('id').inTable('acl_actions').notNullable()
      table.unique(['page_id', 'action_id'])
    })
  }

  down () {
    this.drop('acl_page_actions')
  }
}

module.exports = PageActionsSchema

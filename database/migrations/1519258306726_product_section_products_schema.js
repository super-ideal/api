'use strict'

const Schema = use('Schema')

class ProductSectionProductSchema extends Schema {
  up () {
    this.create('product_section_products', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('product_id').unsigned().references('id').inTable('products').notNullable()
      table.integer('section_id').unsigned().references('id').inTable('product_section').notNullable()
      table.unique(['product_id', 'section_id'])
    })
  }

  down () {
    this.drop('product_section_products')
  }
}

module.exports = ProductSectionProductSchema

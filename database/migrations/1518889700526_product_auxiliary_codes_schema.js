'use strict'

const Schema = use('Schema')

class ProductAuxiliaryCodesSchema extends Schema {
  up () {
    this.create('product_auxiliary_codes', (table) => {
      table.engine('InnoDB')
      table.charset('utf8')
      table.increments()
      table.integer('product_id').unsigned().references('id').inTable('products').notNullable()
      table.string('code', 13).notNullable().unique()
      table.integer('box_factor').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('product_auxiliary_codes')
  }
}

module.exports = ProductAuxiliaryCodesSchema

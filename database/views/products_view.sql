CREATE OR REPLACE VIEW products_view AS
select ppgv.*, oppv.offer_id, oppv.offer_type_name, oppv.price_discount 'offer_price_discount'
from products_price_group_view as ppgv
LEFT JOIN offer_products_progress_view oppv on oppv.product_id = ppgv.id
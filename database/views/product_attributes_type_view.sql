CREATE OR REPLACE VIEW product_attributes_type_view as
select pa.*,
pat.active 'type_active', pat.required 'type_required', pat.public 'type_public', pat.style_id 'type_style_id', pat.name 'type_name', pat.description 'type_description'
from product_attributes as pa
inner join product_attributes_types as pat on pat.id = pa.type_id

CREATE OR REPLACE VIEW auth AS
select us.*, pe.active, pe.name_reason, pe.cpf_cnpj, pe.type_person
from users as us
inner join partners as pe on pe.id = us.partner_id

/*
Mostra todos so produtos em oferta e NÃO mostra os produtos que estão no grupo de preço
*/

CREATE OR REPLACE VIEW offer_products_abstract_view AS
SELECT opv.id 'offer_product_id', p.id 'product_id', o.id 'offer_id',
  opv.offer_type_view, opv.offer_motive_id, opv.active 'active_offer_product', opv.top_offer, opv.type_discount_percentage, opv.type_discount_money, opv.discount, opv.discount_price_two, opv.cumulative, opv.cumulative_payable, opv.wholesale, opv.price_group, opv.limit_per_coupon, opv.view_percent_discount, opv.added_by, opv.updated_by, opv.group_price_attribute_id, opv.group_price_attribute_name,
  o.active 'active_offer',
  om.name AS offer_motive_name,
  p.barcode, p.description AS product_description, p.price, p.active 'active_sale_product',
  padded.name_reason AS added_by_name_reason, pupdated.name_reason AS updated_by_name_reason
FROM offers AS o
INNER JOIN offer_products_view AS opv ON opv.offer_id = o.id
INNER JOIN products AS p ON p.id = opv.product_id
LEFT JOIN offer_products_motives AS om ON om.id = opv.offer_motive_id
INNER JOIN users AS uadded ON uadded.id = opv.added_by
INNER JOIN partners AS padded ON padded.id = uadded.partner_id
LEFT JOIN users AS uupdated ON uupdated.id = opv.updated_by
LEFT JOIN partners AS pupdated ON pupdated.id = uupdated.partner_id

CREATE OR REPLACE VIEW acl_page_action_users_view AS
SELECT apav.*, apau.user_id, uv.username, uv.name_reason
FROM acl_page_action_view apav
RIGHT JOIN acl_page_action_users apau on apau.page_action_id = apav.page_action_id
INNER JOIN users_view uv on uv.id = apau.user_id
ORDER BY apau.user_id, apav.page_name, apav.action_name

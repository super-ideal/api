/*
* Une os produtos que estão em oferta com os produtos do grupo de preço que estão em oferta
*/
CREATE OR REPLACE VIEW offer_products_price_group_abstract_view AS
SELECT pop.offer_id, pop.id 'offer_product_id', pop.product_id FROM offer_products pop
UNION ALL
SELECT popgp.offer_id, popgp.offer_product_id, popgp.product_id FROM offer_products_group_price popgp

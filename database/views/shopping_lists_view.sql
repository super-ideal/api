CREATE OR REPLACE VIEW shopping_lists_view AS
SELECT sl.*, sl.user_id AS created_by_id, p.name_reason AS created_by
FROM shopping_lists AS sl
INNER JOIN users AS u ON u.id = sl.user_id
INNER JOIN partners AS p ON p.id = u.partner_id
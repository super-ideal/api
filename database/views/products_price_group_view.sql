CREATE OR REPLACE VIEW products_price_group_view AS
SELECT p.*,
(CASE WHEN pat.id_unique = 'price_group' THEN pa.id ELSE NULL END) AS 'price_group_id',
(CASE WHEN pat.id_unique = 'price_group' THEN pa.name ELSE NULL END) AS 'price_group_name'
FROM products p
INNER JOIN product_attributes_products pap ON pap.product_id = p.id
INNER JOIN product_attributes pa ON pa.id = pap.attribute_id
INNER JOIN product_attributes_types pat ON pat.id = pa.type_id
group by barcode
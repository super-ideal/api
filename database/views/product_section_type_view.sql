CREATE OR REPLACE VIEW product_section_type_view as
select pa.*, pat.name 'type_name', pat.required 'type_required'
from product_section as pa
inner join product_section_types as pat on pat.id = pa.type_id

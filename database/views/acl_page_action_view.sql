CREATE OR REPLACE VIEW acl_page_action_view AS
SELECT pa.id 'page_action_id', p.tree 'page_tree', p.`level` 'page_level', p.id 'page_id', p.name 'page_name', p.description 'page_description', a.id 'action_id', a.name 'action_name', a.description 'action_description'
FROM acl_page_actions pa
INNER JOIN acl_pages p ON p.id = pa.page_id
INNER JOIN acl_actions a ON a.id = pa.action_id
ORDER BY p.name, a.name

CREATE OR REPLACE VIEW offer_products_price_group_view AS
SELECT opv.id 'offer_product_id', opv.offer_id, opv.offer_motive_id, opv.active 'offer_product_active', opv.top_offer, opv.type_discount_percentage, opv.type_discount_money, opv.discount, opv.discount_price_two, opv.cumulative, opv.cumulative_payable, opv.wholesale, opv.price_group, opv.view_percent_discount, opv.limit_per_coupon, opv.added_by 'offer_product_added_by', opv.updated_by 'offer_product_updated_by', opv.created_at 'offer_product_created_at', opv.updated_at 'offer_product_updated_at', opv.offer_motive_name, opv.added_by_name_reason, opv.updated_by_name_reason, opv.offer_type_view, opv.group_price_attribute_id, opv.group_price_attribute_name,
p.id 'product_id', p.barcode 'product_barcode', p.description 'product_description', p.price 'product_price', p.active 'product_active'
FROM offer_products_price_group_abstract_view AS opgpv
INNER JOIN offer_products_view AS opv ON opv.id = opgpv.offer_product_id
INNER JOIN products AS p ON p.id = opgpv.product_id

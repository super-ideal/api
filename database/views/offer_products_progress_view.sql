/*
Busca todos os produtos de todas as agendas que estão em andamento.
Mostra somente os produtos onde: a agenda está ativa, o produto na agenda está ativo e o produto está ativo para venda.
*/

CREATE OR REPLACE VIEW offer_products_progress_view AS
SELECT opgpv.*, ot.id_unique 'offer_id_unique', ot.name 'offer_type_name', opgpv.discount 'price_discount', o.datetime_end, p.unity_id
FROM offer_products_price_group_view AS opgpv
INNER JOIN offers o ON o.id = opgpv.offer_id
INNER JOIN offer_types ot ON ot.id = o.offer_types_id
INNER JOIN products p ON p.id = opgpv.product_id
WHERE o.active = 1 AND opgpv.offer_product_active = 1 AND opgpv.product_active = 1 AND (o.datetime_start <= NOW() && o.datetime_end >= NOW())
ORDER BY ot.name ASC, opgpv.offer_product_created_at asc
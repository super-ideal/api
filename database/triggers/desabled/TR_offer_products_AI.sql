#Esse trigger será disparado após a inserção de um registro na tabela.

DROP TRIGGER IF EXISTS TR_offer_products_AI;

DELIMITER //
CREATE TRIGGER TR_offer_products_AI
AFTER INSERT
	ON offer_products FOR EACH ROW
BEGIN

	DECLARE group_exists INT DEFAULT 0;
	SELECT (
		# Verifica se  produto que está sendo adicionado já existe outro com o mesmo grupo de preço na oferta
		SELECT COUNT(*) FROM offer_products pop
		INNER JOIN offers po ON po.id = pop.offer_id
		INNER JOIN products p ON p.id = pop.product_id
		INNER JOIN product_attributes_products pap ON pap.product_id = p.id
		INNER JOIN product_attributes pa ON pa.id = pap.attribute_id
		WHERE po.id = NEW.offer_id AND pa.id = (
			# Seleciona o ID do atributo de grupo de preço
			SELECT pa.id
			FROM products p
			INNER JOIN product_attributes_products pap ON pap.product_id = p.id
			INNER JOIN product_attributes pa ON pa.id = pap.attribute_id
			INNER JOIN product_attributes_types pat ON pat.id = pa.type_id
			WHERE p.id = NEW.product_id AND pat.id_unique = 'price_group'
		) LIMIT 1
	) INTO group_exists;

	IF (group_exists = 0 OR group_exists = 1) THEN
		# Somente vai adicionar os demais produto na tabela de grupo de preços em oferta se a opção grupo de preço estiver marcada
		IF (NEW.price_group = 1) THEN
	   	CALL PID_OfferProductGroupPrice(NEW.product_id, NEW.id, NEW.offer_id, 'I');
	   END IF;
	ELSE
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Já existe outro produto com o mesmo grupo de preço nessa agenda.';
	END IF;

END //
DELIMITER ;

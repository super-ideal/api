DROP TRIGGER IF EXISTS TR_offer_products_BU;

DELIMITER $$
	CREATE TRIGGER TR_offer_products_BU BEFORE UPDATE ON offer_products
	FOR EACH ROW BEGIN

		/* Protege contra edição quando a agenda já finalizou */
		IF ((SELECT COUNT(*) FROM offers WHERE datetime_end < NOW() AND id = new.offer_id) != 0) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Não é permitida alteração pois a agenda já foi finalizada.';
		END IF;

	END$$
DELIMITER ;

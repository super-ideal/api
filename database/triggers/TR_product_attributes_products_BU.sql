DROP TRIGGER IF EXISTS TR_product_attributes_products_BU;

DELIMITER $$
CREATE TRIGGER TR_product_attributes_products_BU
BEFORE UPDATE
	ON product_attributes_products FOR EACH ROW
BEGIN

	/* Protege a edição do grupo de preço quando o produto está em oferta */

	# Verifica se o novo attribute_id é diferente do antigo
	IF (NEW.attribute_id != OLD.attribute_id) THEN
		# Verifica se o produto está em oferta
		IF ((select count(*) from offer_products_progress_view where product_id = OLD.product_id) != 0) THEN
			#Verifica se o antigo atributo é um grupo de preço
			IF ((select count(*) from product_attributes_products pap inner join product_attributes pa on pa.id = pap.attribute_id inner join product_attributes_types pat on pat.id = pa.type_id where pat.id_unique = 'price_group' and pap.product_id = OLD.product_id) != 0) THEN
				#Verifica se o novo atributo é um grupo de preço
				IF ((select count(*) from product_attributes_products pap inner join product_attributes pa on pa.id = pap.attribute_id inner join product_attributes_types pat on pat.id = pa.type_id where pat.id_unique = 'price_group' and pap.product_id = NEW.product_id) != 0) THEN
					SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Não é permitida alteração do grupo de preço durante uma oferta.';
				END IF;
			END IF;
		END IF;
	END IF;

END$$
DELIMITER ;

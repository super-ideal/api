DROP TRIGGER IF EXISTS TR_products_BU;

DELIMITER $$
CREATE TRIGGER TR_products_BU
BEFORE UPDATE
	ON products FOR EACH ROW
BEGIN

	IF (OLD.price != NEW.price) THEN
		/* Protege a edição do preço quando o produto está em oferta */
		IF ((select count(*) from offer_products_progress_view where product_id = OLD.id) != 0) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Não é permitida alteração do preço de venda durante uma oferta.';
		END IF;
	END IF;

END$$
DELIMITER ;

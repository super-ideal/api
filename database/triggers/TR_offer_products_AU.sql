DROP TRIGGER IF EXISTS TR_offer_products_AU;

DELIMITER //
CREATE TRIGGER TR_offer_products_AU
AFTER UPDATE
	ON offer_products FOR EACH ROW
BEGIN
	/*IF (NEW.price_group = 1 AND OLD.price_group = 0) THEN
		CALL PID_OfferProductGroupPrice(NEW.product_id, NEW.id, NEW.offer_id, 'I');
	ELSEIF (NEW.price_group = 0 AND OLD.price_group = 1) THEN
		CALL PID_OfferProductGroupPrice(NEW.product_id, NEW.id, NEW.offer_id, 'D');
	END IF;*/
  IF (NEW.price_group = 0 AND OLD.price_group = 1) THEN
		CALL PID_OfferProductGroupPrice(NEW.product_id, NEW.id, NEW.offer_id, 'D');
	END IF;
END //
DELIMITER ;

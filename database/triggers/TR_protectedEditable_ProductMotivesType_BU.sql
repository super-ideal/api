DROP TRIGGER IF EXISTS TR_protectedEditable_ProductMotivesType_BU;

DELIMITER $$
	CREATE TRIGGER TR_protectedEditable_ProductMotivesType_BU BEFORE UPDATE ON offer_products_motives
	FOR EACH ROW BEGIN
		IF (old.editable = 0) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Não é permitida alteração nesse dado.';
		END IF;
	END$$
DELIMITER ;

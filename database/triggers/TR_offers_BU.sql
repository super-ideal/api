DROP TRIGGER IF EXISTS TR_offers_BU;

DELIMITER $$
	CREATE TRIGGER TR_offers_BU BEFORE UPDATE ON offers
	FOR EACH ROW BEGIN
		/* Protege a agenda que já finalizou, contra edição */
		IF (OLD.datetime_end < NOW()) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Não é permitida alteração nessa agenda, pois ela já foi finalizada.';
		/* Protege a agenda que já iniciou, contra edição de alguns campos */
		ELSEIF (OLD.datetime_start <= NOW() OR OLD.offer_types_id != NEW.offer_types_id) THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Não é permitida alteração no Tipo da Oferta ou Data de Início nessa agenda, pois ela já foi iniciada.';
		END IF;
	END$$
DELIMITER ;

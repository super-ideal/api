#Esse trigger será disparado após a exclusão de um registro na tabela.

DROP TRIGGER IF EXISTS TR_offer_products_AD;

DELIMITER //
CREATE TRIGGER TR_offer_products_AD
AFTER DELETE
	ON offer_products FOR EACH ROW
BEGIN
   CALL PID_OfferProductGroupPrice(OLD.product_id, OLD.id, OLD.offer_id, 'D');
END //
DELIMITER ;

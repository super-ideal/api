'use strict'

/*
|--------------------------------------------------------------------------
| ProductAttributesTypeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class ProductAttributesTypeSeeder {
  async run() {
    const model = [
      { active: 1, id_unique: 'price_group', editable: 0, public: 0, style_id: 'dropdown', required: 0, name: 'Grupo de Preço', description: 'O preço de venda de todos os produtos com esse atributo, serão sincronizados automaticamente.' },
      { active: 1, id_unique: 'brand', editable: 0, public: 1, style_id: 'dropdown', required: 1, name: 'Marca', description: 'Especifica a marca do produto.' },
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Product/Attributes/AttributesType').create({ data: entry })
    }
  }
}

module.exports = ProductAttributesTypeSeeder

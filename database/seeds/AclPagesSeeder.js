'use strict'

/*
|--------------------------------------------------------------------------
| AclPagesSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class AclPagesSeeder {
  async run() {
    const model = [
      { name: 'PERMISSIONS', description: 'Permissões', tree: null, level: null },
      { name: 'USERS', description: 'Usuários', tree: null, level: null },
      { name: 'PARTNERS', description: 'Parceiros', tree: null, level: null },
      { name: 'PARTNER_ADRESSES', description: 'Seçãos', tree: 3, level: 1 },
      { name: 'PARTNER_EMAILS', description: 'E-mails', tree: 3, level: 1 },
      { name: 'PARTNER_PHONES', description: 'Telefones', tree: 3, level: 1 },
      { name: 'PRODUCTS', description: 'Produtos', tree: null, level: null },
      { name: 'PRODUCT_ADDRESS', description: 'Setores', tree: 7, level: 1 },
      { name: 'PRODUCT_ADDRESS_TYPES', description: 'Tipos', tree: 8, level: 2 },
      { name: 'PRODUCT_ATTRIBUTES', description: 'Atributos', tree: 7, level: 1 },
      { name: 'PRODUCT_ATTRIBUTES_TYPES', description: 'Tipos', tree: 10, level: 2 },
      { name: 'OFFERS', description: 'Agenda de Ofertas', tree: null, level: null },
      { name: 'OFFERS_MOTIVES', description: 'Motivos de Oferta', tree: 12, level: 1 },
      { name: 'OFFERS_TYPES', description: 'Tipos de Oferta', tree: 12, level: 1 },
      { name: 'CONTROL_PANEL', description: 'Painel de Controle', tree: null, level: null },
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Acl/Page').create({ data: entry })
    }
  }
}

module.exports = AclPagesSeeder

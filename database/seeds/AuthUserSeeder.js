'use strict'

/*
|--------------------------------------------------------------------------
| AuthUserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class AuthUserSeeder {
  async run () {
    //const partner = await Factory.model('App/Models/Partner/Partner').create()
    //const user = await Factory.model('App/Models/User/User').make({ status: 'admin' })

    //await partner.user().save(user)
    await Factory.model('App/Models/User/User').create({ status: 'admin' })
  }
}

module.exports = AuthUserSeeder

'use strict'

/*
|--------------------------------------------------------------------------
| AclPagesSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class AclPagesActionsSeeder {
  async run() {
    /*
    1 - VIEW_ONE
    2 - VIEW_ALL
    3 - NEW
    4 - EDIT
    5 - DELETE
    6 - ACCESS
    */

    const model = [
      { page_id: 1, action_id: 1 },
      { page_id: 1, action_id: 4 },
      { page_id: 1, action_id: 6 },
      { page_id: 2, action_id: 1 },
      { page_id: 2, action_id: 2 },
      { page_id: 2, action_id: 3 },
      { page_id: 2, action_id: 4 },
      { page_id: 3, action_id: 1 },
      { page_id: 3, action_id: 2 },
      { page_id: 3, action_id: 3 },
      { page_id: 3, action_id: 4 },
      { page_id: 4, action_id: 3 },
      { page_id: 4, action_id: 4 },
      { page_id: 4, action_id: 5 },
      { page_id: 5, action_id: 3 },
      { page_id: 5, action_id: 4 },
      { page_id: 5, action_id: 5 },
      { page_id: 6, action_id: 3 },
      { page_id: 6, action_id: 4 },
      { page_id: 6, action_id: 5 },
      { page_id: 7, action_id: 1 },
      { page_id: 7, action_id: 2 },
      { page_id: 7, action_id: 3 },
      { page_id: 7, action_id: 4 },
      { page_id: 8, action_id: 1 },
      { page_id: 8, action_id: 2 },
      { page_id: 8, action_id: 3 },
      { page_id: 8, action_id: 4 },
      { page_id: 9, action_id: 1 },
      { page_id: 9, action_id: 2 },
      { page_id: 9, action_id: 3 },
      { page_id: 9, action_id: 4 },
      { page_id: 10, action_id: 1 },
      { page_id: 10, action_id: 2 },
      { page_id: 10, action_id: 3 },
      { page_id: 10, action_id: 4 },
      { page_id: 11, action_id: 1 },
      { page_id: 11, action_id: 2 },
      { page_id: 11, action_id: 3 },
      { page_id: 11, action_id: 4 },
      { page_id: 12, action_id: 1 },
      { page_id: 12, action_id: 2 },
      { page_id: 12, action_id: 3 },
      { page_id: 12, action_id: 4 },
      { page_id: 13, action_id: 1 },
      { page_id: 13, action_id: 2 },
      { page_id: 13, action_id: 3 },
      { page_id: 13, action_id: 4 },
      { page_id: 14, action_id: 1 },
      { page_id: 14, action_id: 2 },
      { page_id: 14, action_id: 3 },
      { page_id: 14, action_id: 4 },
      { page_id: 15, action_id: 6 },
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Acl/PageAction').create({ data: entry })
    }
  }
}

module.exports = AclPagesActionsSeeder

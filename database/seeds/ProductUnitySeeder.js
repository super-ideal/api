'use strict'

/*
|--------------------------------------------------------------------------
| ProductUnitySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class ProductUnitySeeder {
  async run () {
    const model = [
      { name: 'Unidade', abbreviation: 'und' },
      { name: 'Caixa', abbreviation: 'cx' },
      { name: 'Fardo', abbreviation: 'frd' },
      { name: 'Pacote', abbreviation: 'pct' },
      { name: 'Saco', abbreviation: 'sc' },
      { name: 'Litro', abbreviation: 'l' },
      { name: 'Kilograma', abbreviation: 'kg' },
      { name: 'Grama', abbreviation: 'g' },
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Product/Unity').create({ data: entry })
    }
  }
}

module.exports = ProductUnitySeeder

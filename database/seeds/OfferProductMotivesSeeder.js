'use strict'

/*
|--------------------------------------------------------------------------
| OfferProductMotivesSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class OfferProductMotivesSeeder {
  async run() {
    const model = [
      { editable: 0, active: 1, name: 'Rebaixa por vencimento' },
      { editable: 0, active: 1, name: 'Rebaixa por excesso' },
      { editable: 0, active: 1, name: 'Rebaixa sem giro' }
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Offer/OfferMotive').create({ data: entry })
    }
  }
}

module.exports = OfferProductMotivesSeeder

'use strict'

/*
|--------------------------------------------------------------------------
| ProductSectionTypeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class ProductSectionSeeder {
  async run() {
    const model = [
      { active: 1, name: 'Alimentos', parent_id: null, description: null, type_id: 1}, // 1
      { active: 1, name: 'Automotivos', parent_id: null, description: null, type_id: 1}, // 2
      { active: 1, name: 'Bebês', parent_id: null, description: null, type_id: 1}, // 3
      { active: 1, name: 'Bebidas comuns', parent_id: null, description: null, type_id: 1}, // 4
      { active: 1, name: 'Bebidas alcóolicas', parent_id: null, description: null, type_id: 1}, // 5
      { active: 1, name: 'Bomboniere', parent_id: null, description: null, type_id: 1}, // 6
      { active: 1, name: 'Carnes', parent_id: null, description: null, type_id: 1}, // 7
      { active: 1, name: 'Condimentos e conservas', parent_id: null, description: null, type_id: 1}, // 8
      { active: 1, name: 'Congelados', parent_id: null, description: null, type_id: 1}, // 9
      { active: 1, name: 'Eletro', parent_id: null, description: null, type_id: 1}, // 10
      { active: 1, name: 'Farinhas e grãos', parent_id: null, description: null, type_id: 1}, // 11
      { active: 1, name: 'Farmácia', parent_id: null, description: null, type_id: 1}, // 12
      { active: 1, name: 'Feira', parent_id: null, description: null, type_id: 1}, // 13
      { active: 1, name: 'Finlandek', parent_id: null, description: null, type_id: 1}, // 14
      { active: 1, name: 'Frios', parent_id: null, description: null, type_id: 1}, // 15
      { active: 1, name: 'Higiene bucal', parent_id: null, description: null, type_id: 1}, // 16
      { active: 1, name: 'Limpeza', parent_id: null, description: null, type_id: 1}, // 17
      { active: 1, name: 'Matinais', parent_id: null, description: null, type_id: 1}, // 18
      { active: 1, name: 'Natal', parent_id: null, description: null, type_id: 1}, // 19
      { active: 1, name: 'Padaria', parent_id: null, description: null, type_id: 1}, // 20
      { active: 1, name: 'Perfumaria', parent_id: null, description: null, type_id: 1}, // 21
      { active: 1, name: 'Pet', parent_id: null, description: null, type_id: 1}, // 22
      { active: 1, name: 'Resfriados', parent_id: null, description: null, type_id: 1}, // 23
      { active: 1, name: 'Sobremesas', parent_id: null, description: null, type_id: 1}, // 24
      { active: 1, name: 'Alimentação infantil', parent_id: 3, description: null, type_id: 2},
      { active: 1, name: 'Cuidados para o corpo', parent_id: 3, description: null, type_id: 2},
      { active: 1, name: 'Shampoo infantil', parent_id: 3, description: null, type_id: 2},
      { active: 1, name: 'Lenços umedecidos', parent_id: 3, description: null, type_id: 2},
      { active: 1, name: 'Condicionador e creme para os cabelos', parent_id: 3, description: null, type_id: 2},
      { active: 1, name: 'Fraldas', parent_id: 3, description: null, type_id: 2},
      { active: 1, name: 'Escova dental infantil', parent_id: 3, description: null, type_id: 2},
      { active: 1, name: 'Absorventes', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Algodão e cotonetes', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Condicionadores', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Desodorantes masculino', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Desodorantes feminino', parent_id: 21, description: null, type_id: 2 },
      { active: 1, name: 'Geriátricas', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Kits para aparelhos de barba', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Limpeza e tonificação facial', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Protetor solar', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Shampoos anticaspa e 2 em 1', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Tratamentos e reparações', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Acessórios para banho', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Barba', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Cremes pré e pós-barba', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Drogaria', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Hidratação facial', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Kits shampoo', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Modeladores', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Repelentes', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Shampoos', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Acessórios para depilação', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Condicionadores anticaspa', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Fixadores', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Hidratantes e óleos corporais', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Lenços', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Papel higiênico', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Sabonetes', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Talcos', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Creme dental', parent_id: 21, description: null, type_id: 2},
      { active: 1, name: 'Alvejante e água sanitária', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Detergentes', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Sabão líquido', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Desengordurantes e limpa alumínio', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Ceras e removedores', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Acessórios de banheiro', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Aromatização e purificação do ar', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Luvas e panos de limpeza', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Pilhas e baterias', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Utensílios para churrasco', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Amaciantes', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Sabão em pó', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Sabão para cozinha', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Limpadores para banheiro', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Saco para lixo', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Vassouras e rodos', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Acessórios para cozinha', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Organizadores', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Jardinagem', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Tira manchas', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Sabão em barra', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Desinfetantes para banheiro', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Limpadores para sala', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Inseticidas', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Lixeiras e baldes', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Desodorizador para banheiro', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Lâmpadas', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Reparos domésticos', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Outros utensílios para banheiro', parent_id: 17, description: null, type_id: 2},
      { active: 1, name: 'Açúcar', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Arroz', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Cremes', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Massas com ovos e massas de sêmola', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Massas para lasanha', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Óleos', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Sopão', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Sopas individuais', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Adoçantes', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Feijão', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Massas caseiras e integrais', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Massas de grano duro', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Macarrão instantâneo', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Sal', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Sopas prontas', parent_id: 1, description: null, type_id: 2},
      { active: 1, name: 'Vinagres e agrin', parent_id: 1, description: null, type_id: 2},
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Product/Section/Section').create({ data: entry })
    }
  }
}

module.exports = ProductSectionSeeder

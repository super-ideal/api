'use strict'

/*
|--------------------------------------------------------------------------
| AclPagesSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')

class AclPagesSeeder {
  async run() {
    const model = [
      { name: 'VIEW_ONE', description: 'Visualizar um' }, // ID: 1
      { name: 'VIEW_ALL', description: 'Visualizar todos' }, // ID: 2
      { name: 'NEW', description: 'Novo/Gravar' }, // ID: 3
      { name: 'EDIT', description: 'Editar/Atualizar' }, // ID: 4
      { name: 'DELETE', description: 'Excluir' }, // ID: 5
      { name: 'ACCESS', description: 'Acessar' } // ID: 6
    ]

    for (let entry of model) {
      await Factory.model('App/Models/Acl/Action').create({ data: entry })
    }
  }
}

module.exports = AclPagesSeeder

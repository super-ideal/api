DROP PROCEDURE IF EXISTS PU_AtualizaPrecoGrupoPreco;

DELIMITER $$
CREATE PROCEDURE PU_AtualizaPrecoGrupoPreco (id_product int, new_price decimal(8,2), old_price decimal(8,2))
BEGIN
	DECLARE pDONE INT DEFAULT 0;
	DECLARE pID INT;

	DECLARE curs CURSOR FOR (
		SELECT p.id FROM products p
		INNER JOIN product_attributes_products pap on pap.product_id = p.id
		INNER JOIN product_attributes pa on pa.id = pap.attribute_id
		INNER JOIN product_attributes_types pat on pat.id = pa.type_id
		WHERE pat.id_unique = 'price_group'
			AND pap.attribute_id = (
				SELECT pap.attribute_id
				FROM products p
				INNER JOIN product_attributes_products pap ON pap.product_id = p.id
				INNER JOIN product_attributes pa on pa.id = pap.attribute_id
				INNER JOIN product_attributes_types pat on pat.id = pa.type_id
				WHERE pap.product_id = id_product and pat.id_unique = 'price_group'
			) #AND pap.product_id <> id_product
	);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET pDONE=1;

	OPEN curs;
		SET pDONE = 0;
		REPEAT FETCH curs INTO pID;
			IF new_price != old_price THEN
		   	UPDATE products SET price = new_price WHERE id = pID;

        /*UPDATE products as p
        INNER JOIN product_attributes_products pap on pap.product_id = p.id
        INNER JOIN product_attributes pa on pa.id = pap.attribute_id
        INNER JOIN product_attributes_types pat on pat.id = pa.type_id
        SET price = 30
        WHERE pat.id_unique = 'price_group' AND pap.attribute_id = 2;*/
			END IF;
		UNTIL pDONE END REPEAT;
	CLOSE curs;
END $$
DELIMITER ;

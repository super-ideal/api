DROP PROCEDURE IF EXISTS PID_OfferProductGroupPrice;

DELIMITER $$
CREATE PROCEDURE PID_OfferProductGroupPrice (id_product INT, id_offer_product INT,  id_offer INT, typeProccess VARCHAR(30))
BEGIN
	DECLARE done INT DEFAULT 0;
	DECLARE pIDProduct INT;

	DECLARE curs CURSOR FOR (
		#lista todos os produtos do grupo de preço especifico
		select p.id as 'product_id'
		from products p
		inner join product_attributes_products pap on pap.product_id = p.id
		inner join product_attributes pa on pa.id = pap.attribute_id
		inner join product_attributes_types pat on pat.id = pa.type_id
		where p.id != id_product and pat.id_unique = 'price_group' and pa.id = (
			#descobrir id do atributo
			select pa.id
			from products p
			inner join product_attributes_products pap on pap.product_id = p.id
			inner join product_attributes pa on pa.id = pap.attribute_id
			inner join product_attributes_types pat on pat.id = pa.type_id
			where p.id = id_product and pat.id_unique = 'price_group' LIMIT 1
		)
	);
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=1;

	SET done = 0;
	OPEN curs;
		REPEAT FETCH curs INTO pIDProduct;
			IF done != 1 THEN
				IF (typeProccess = 'I') THEN
					INSERT INTO offer_products_group_price (product_id, offer_product_id, offer_id) VALUES (pIdProduct, id_offer_product, id_offer);
				ELSEIF (typeProccess = 'D') THEN
					DELETE FROM offer_products_group_price WHERE offer_product_id = id_offer_product;
				END IF;
			END IF;
		UNTIL done END REPEAT;
	CLOSE curs;
END $$
DELIMITER ;

'use strict'

const Route = use('Route')

/**
 * Middlewares
 * - throttle: determina qual a quantidade de solicitações o usuário pode fazer em um deternado tempo
 * - auth: verifica se o usuário esta autenticado. Parametros: (auth:TIPO_AUTENTICAÇÃO) Default: JWT
 * - check: verifica se o usuário tem permissão para continuar. Parametros: (check:PAGE,AÇÃO)
 * - status: verifica se o status do usuário permite ele continuar. Parametros: (status:STATUS_USUARIO) Default: admin
*/

Route.get('/', ({ request }) => {
  return { greeting: 'Hello world in JSON' }
})

Route.group(() => {
  Route.post('login', 'Auth/AuthController.postLogin').middleware(['throttle:5,60'])
  Route.get('name', 'Auth/AuthController.getName').middleware(['throttle:10,120'])
  Route.post('register', 'Auth/AuthController.register').middleware(['throttle:5'])

  Route.post('refresh', 'Auth/AuthController.postRefreshToken')
  Route.get('logout', 'Auth/AuthController.getLogoutAll').middleware(['auth:jwt_email'])
  Route.get('me', 'Auth/AuthController.getUserLogged').middleware(['throttle:10,60', 'auth:jwt_email'])

  Route.get('password/reset/:token', 'User/PasswordResetController.show')
  Route.post('password/reset', 'User/PasswordResetController.reset').validator('PasswordResetUser')
  Route.post('password/forgot', 'User/ForgotPasswordController.sendResetLinkEmail')//.validator('ForgotPasswordUser')

  //Route.get('tokens', 'AuthController.getTokens').middleware(['throttle:5', 'auth:jwt'])
  //Route.get(':provider', 'AuthController.getProviderRedirect')
  //Route.get(':provider/callback', 'AuthController.getProviderCallback')
  //Route.get('/forgotPassword', 'AuthController.forgotPassword').as('forgotPassword')
}).prefix('v1/auth')

Route.group(() => {
  // Partner Adresses
  // Route.get('partners/adresses', 'Partner/PartnerAddressController.index').middleware(['throttle:30,60', 'check:PARTNERS,VIEW_ONE'])
  // Route.get('partners/adresses/:id', 'Partner/PartnerAddressController.show').middleware(['throttle:30,60', 'check:PARTNERS,VIEW_ONE'])
  Route.post('partners/adresses/:idPartner', 'Partner/PartnerAddressController.store').middleware(['throttle:5,60', 'check:PARTNER_ADRESSES,NEW'])
  Route.patch('partners/adresses/:id', 'Partner/PartnerAddressController.update').middleware(['throttle:5,60', 'check:PARTNER_ADRESSES,EDIT'])
  Route.delete('partners/adresses/:id', 'Partner/PartnerAddressController.delete').middleware(['throttle:5,60', 'check:PARTNER_ADRESSES,DELETE'])

  // Partner Phones
  //Route.get('partners/phones', 'Partner/PartnerPhoneController.index').middleware(['throttle:30,60'])
  //Route.get('partners/phones/:id', 'Partner/PartnerPhoneController.show').middleware(['throttle:30,60'])
  Route.post('partners/phones/:idPartner', 'Partner/PartnerPhoneController.store').middleware(['throttle:5,60', 'check:PARTNER_PHONES,NEW'])
  Route.patch('partners/phones/:id', 'Partner/PartnerPhoneController.update').middleware(['throttle:5,60', 'check:PARTNER_PHONES,EDIT'])
  Route.delete('partners/phones/:id', 'Partner/PartnerPhoneController.delete').middleware(['throttle:5,60', 'check:PARTNER_PHONES,DELETE'])

  // Partner Emails
  //Route.get('partners/emails', 'Partner/PartnerEmailController.index').middleware(['throttle:30,60'])
  //Route.get('partners/emails/:id', 'Partner/PartnerEmailController.show').middleware(['throttle:30,60'])
  Route.post('partners/emails/:idPartner', 'Partner/PartnerEmailController.store').middleware(['throttle:5,60', 'check:PARTNER_EMAILS,NEW'])
  Route.patch('partners/emails/:id', 'Partner/PartnerEmailController.update').middleware(['throttle:5,60', 'check:PARTNER_EMAILS,EDIT'])
  Route.delete('partners/emails/:id', 'Partner/PartnerEmailController.delete').middleware(['throttle:5,60', 'check:PARTNER_EMAILS,DELETE'])

  // Partner
  Route.get('partners/types', 'Partner/PartnerController.typesPartners').middleware(['throttle:30,60'])
  Route.get('partners', 'Partner/PartnerController.index').middleware(['throttle:30,60', 'check:PARTNERS,VIEW_ALL'])
  Route.get('partners/:id', 'Partner/PartnerController.show').middleware(['throttle:30,60', 'check:PARTNERS,VIEW_ONE'])
  Route.post('partners', 'Partner/PartnerController.store').middleware(['throttle:5,60', 'check:PARTNERS,NEW'])
  Route.patch('partners/:id', 'Partner/PartnerController.update').middleware(['throttle:5,60', 'check:PARTNERS,EDIT'])

  // User
  Route.get('users', 'User/UserController.index').middleware(['throttle:5,60', 'check:USERS,VIEW_ALL'])
  Route.post('users', 'User/UserController.store').middleware(['throttle:5,60', 'check:USERS,NEW'])
  Route.get('users/:id', 'User/UserController.show').middleware(['throttle:30,60', 'check:USERS,VIEW_ONE'])
  //Route.get('users/:id/acl', 'User/UserController.permissionMany')
  //Route.get('users/:id/acl/:page/:action', 'User/UserController.permissionOne')
  Route.patch('users/:id', 'User/UserController.update').middleware(['throttle:5,60', 'check:USERS,EDIT'])

  // Acl Access Pages
  Route.get('acl/page', 'Acl/PageAclController.indexPage').middleware(['throttle:30,60'])
  //Route.post('acl/page', 'Acl/PageAclController.storePage').middleware(['throttle:5,60', 'status:ADMIN'])
  //Route.delete('acl/page/:id', 'Acl/PageAclController.deletePage').middleware(['throttle:5,60', 'status:ADMIN'])

  //Route.get('acl/action', 'Acl/ActionAclController.indexAction').middleware(['throttle:30,60', 'status:ADMIN'])
  //Route.post('acl/action', 'Acl/ActionAclController.storeAction').middleware(['throttle:5,60', 'status:ADMIN'])
  //Route.delete('acl/action/:id', 'Acl/ActionAclController.deleteAction').middleware(['throttle:5,60', 'status:ADMIN'])

  //Route.post('acl/action/page', 'Acl/PageActionAclController.storeActionPage').middleware(['throttle:5,60', 'status:ADMIN'])
  //Route.delete('acl/action/page/:id', 'Acl/PageActionAclController.deleteActionPage').middleware(['throttle:5,60'])
  Route.get('acl/action/page', 'Acl/PageActionAclController.allPageAction').middleware(['throttle:30,60'])
  //Route.get('acl/action/page/:idPage', 'Acl/PageActionAclController.showActionsPage').middleware(['throttle:30,60', 'status:ADMIN'])

  Route.get('acl/user/:idUser', 'Acl/UserAclController.showPermissionsUserId').middleware(['throttle:30,60', 'check:PERMISSIONS,VIEW_ONE'])
  // Route.get('acl/user', 'Acl/UserAclController.showPermissionsUser').middleware(['throttle:30,60', 'check:PERMISSIONS,VIEW_ONE'])
  Route.patch('acl/user/:idUser', 'Acl/UserAclController.update').middleware(['throttle:30,60', 'check:PERMISSIONS,EDIT'])

  // Products - Auxiliare Code
  //Route.post('products/auxiliarecodes', 'Product/AuxiliaryCodeController.store').middleware(['throttle:5,60'])
  //Route.get('products/auxiliarecodes/product/:product_id', 'Product/AuxiliaryCodeController.getAll').middleware(['throttle:30,60'])
  //Route.get('products/auxiliarecodes/code/:code', 'Product/AuxiliaryCodeController.getCode').middleware(['throttle:30,60'])
  //Route.patch('products/auxiliarecodes/:id', 'Product/AuxiliaryCodeController.update').middleware(['throttle:5,60'])
  //Route.delete('products/auxiliarecodes/:id', 'Product/AuxiliaryCodeController.delete').middleware(['throttle:5,60'])

  // Products - Weight
  //Route.post('products/weights', 'Product/WeightController.store').middleware(['throttle:5,60'])
  //Route.get('products/weights/:idProduct', 'Product/WeightController.show').middleware(['throttle:30,60'])
  //Route.patch('products/weights/:idProduct', 'Product/WeightController.update').middleware(['throttle:5,60'])

  // Estilo do tipo de atributo
  Route.get('products/attributes/types/styles', 'Product/Attributes/AttributesTypeStyleController.index').middleware(['throttle:30,60'])

  // Products/Attributes/Types - Tipos de Atributos
  Route.post('products/attributes/types', 'Product/Attributes/AttributesTypeController.store').middleware(['throttle:5,60', 'check:PRODUCT_ATTRIBUTES_TYPES,NEW'])
  Route.get('products/attributes/types', 'Product/Attributes/AttributesTypeController.index').middleware(['throttle:30,60', 'check:PRODUCT_ATTRIBUTES_TYPES,VIEW_ALL'])
  Route.get('products/attributes/types/:idType', 'Product/Attributes/AttributesTypeController.show').middleware(['throttle:30,60', 'check:PRODUCT_ATTRIBUTES_TYPES,VIEW_ONE'])
  Route.patch('products/attributes/types/:idType', 'Product/Attributes/AttributesTypeController.update').middleware(['throttle:5,60', 'check:PRODUCT_ATTRIBUTES_TYPES,EDIT'])

  // Products/Attributes - Atributos dos Produtos
  Route.post('products/attributes', 'Product/Attributes/AttributesController.store').middleware(['throttle:5,60', 'check:PRODUCT_ATTRIBUTES,NEW'])
  Route.get('products/attributes', 'Product/Attributes/AttributesController.index').middleware(['throttle:30,60', 'check:PRODUCT_ATTRIBUTES,VIEW_ALL'])
  Route.get('products/attributes/:idAttribute', 'Product/Attributes/AttributesController.show').middleware(['throttle:30,60', 'check:PRODUCT_ATTRIBUTES,VIEW_ONE'])
  Route.patch('products/attributes/:idAttribute', 'Product/Attributes/AttributesController.update').middleware(['throttle:5,60', 'check:PRODUCT_ATTRIBUTES,EDIT'])
  //Route.delete('products/attributes/:idAttribute', 'Product/Attributes/AttributesController.delete').middleware(['throttle:5,60', 'check:PRODUCT_ATTRIBUTES,DELETE'])
  //Route.get('products/attributes/:idType/types', 'Product/Attributes/AttributesController.searchAttributesByTypes').middleware(['throttle:30,60'])

  // Products/Section - Tipos de Setores
  Route.post('products/section/types', 'Product/Section/SectionTypeController.store').middleware(['throttle:5,60', 'check:PRODUCT_ADDRESS_TYPES,NEW'])
  Route.get('products/section/types', 'Product/Section/SectionTypeController.index').middleware(['throttle:30,60', 'check:PRODUCT_ADDRESS_TYPES,VIEW_ALL'])
  Route.get('products/section/types/:idType', 'Product/Section/SectionTypeController.show').middleware(['throttle:30,60', 'check:PRODUCT_ADDRESS_TYPES,VIEW_ONE'])
  Route.patch('products/section/types/:idType', 'Product/Section/SectionTypeController.update').middleware(['throttle:5,60', 'check:PRODUCT_ADDRESS_TYPES,EDIT'])
  //Route.delete('products/section/types/:idType', 'Product/Section/SectionTypeController.delete').middleware(['throttle:5,60'])

  // Products/Section - Setores de Produtos
  Route.post('products/section', 'Product/Section/SectionController.store').middleware(['throttle:5,60', 'check:PRODUCT_ADDRESS,NEW'])
  Route.get('products/section', 'Product/Section/SectionController.index').middleware(['throttle:30,60', 'check:PRODUCT_ADDRESS,VIEW_ALL'])
  Route.get('products/section/:idSection', 'Product/Section/SectionController.show').middleware(['throttle:30,60', 'check:PRODUCT_ADDRESS,VIEW_ONE'])
  Route.patch('products/section/:idSection', 'Product/Section/SectionController.update').middleware(['throttle:5,60', 'check:PRODUCT_ADDRESS,EDIT'])
  //Route.delete('products/section/:idSection', 'Product/Section/SectionController.delete').middleware(['throttle:5,60'])
  //Route.get('products/section/:idType/types', 'Product/Section/SectionController.searchSectionByTypes').middleware(['throttle:30,60'])

  // Products/Images - Imagens dos Produtos
  //Route.get('products/:idProduct/images', 'Product/ImageController.index').middleware(['throttle:30,60'])
  //Route.get('products/images/:idImage', 'Product/ImageController.show').middleware(['throttle:30,60'])
  //Route.post('products/:idProduct/images', 'Product/ImageController.store').middleware(['throttle:5,60'])
  //Route.patch('products/images/:idImage', 'Product/ImageController.update').middleware(['throttle:5,60'])
  Route.delete('products/images/:idImage', 'Product/ImageController.delete').middleware(['throttle:5,60', 'check:PRODUCTS,EDIT'])

  Route.get('products/unitys', 'Product/UnityController.index').middleware(['throttle:30,60'])

  // Products - Produtos
  Route.post('products', 'Product/ProductController.store').middleware(['throttle:5,60', 'check:PRODUCTS,NEW'])
  Route.get('products', 'Product/ProductController.index').middleware(['throttle:30,60', 'check:PRODUCTS,VIEW_ALL'])
  Route.get('products/:idProduct', 'Product/ProductController.show').middleware(['throttle:30,60', 'check:PRODUCTS,VIEW_ONE'])
  Route.patch('products/:idProduct', 'Product/ProductController.update').middleware(['throttle:5,60', 'check:PRODUCTS,EDIT'])
  
  Route.get('products/barcode/:barcode', 'Product/ProductController.verifyBarcodeDuplicated')
  Route.get('products/groupprice/:idAttributeProduct', 'Product/ProductController.productsGroupPrice').middleware(['throttle:5,60'])

  // Offers/Types - Tipos de Agenda
  Route.post('offers/types', 'Offer/OfferTypeController.store').middleware(['throttle:5,60', 'check:OFFERS_TYPES,NEW'])
  Route.get('offers/types', 'Offer/OfferTypeController.index').middleware(['throttle:30,60', 'check:OFFERS_TYPES,VIEW_ALL'])
  Route.get('offers/types/:idType', 'Offer/OfferTypeController.show').middleware(['throttle:30,60', 'check:OFFERS_TYPES,VIEW_ONE'])
  Route.patch('offers/types/:idType', 'Offer/OfferTypeController.update').middleware(['throttle:5,60', 'check:OFFERS_TYPES,EDIT'])
  //Route.delete('offers/types/:idType', 'Offer/OfferTypeController.delete').middleware(['throttle:5,60'])

  // Offer - Agenda Ofertas
  Route.post('offers', 'Offer/OfferController.store').middleware(['throttle:5,60', 'check:OFFERS,NEW'])
  Route.get('offers', 'Offer/OfferController.index').middleware(['throttle:30,60', 'check:OFFERS,VIEW_ALL'])
  Route.get('offers/:idOffer', 'Offer/OfferController.show').middleware(['throttle:30,60', 'check:OFFERS,VIEW_ONE'])
  Route.patch('offers/:idOffer', 'Offer/OfferController.update').middleware(['throttle:5,60', 'check:OFFERS,EDIT'])
  //Route.delete('offers/:idOffer', 'Offer/OfferController.delete').middleware(['throttle:5,60'])

  // Offers/Products/Motives - Motivos de um produto em uma Agenda
  Route.post('offers/motives/product', 'Offer/OfferProductMotiveController.store').middleware(['throttle:5,60', 'check:OFFERS_MOTIVES,NEW'])
  Route.get('offers/motives/product', 'Offer/OfferProductMotiveController.index').middleware(['throttle:30,60', 'check:OFFERS_MOTIVES,VIEW_ALL'])
  Route.get('offers/motives/:idMotive/product', 'Offer/OfferProductMotiveController.show').middleware(['throttle:30,60', 'check:OFFERS_MOTIVES,VIEW_ONE'])
  Route.patch('offers/motives/:idMotive/product', 'Offer/OfferProductMotiveController.update').middleware(['throttle:5,60', 'check:OFFERS_MOTIVES,EDIT'])
  //Route.delete('offers/motives/:idMotive/product', 'Offer/OfferProductMotiveController.delete').middleware(['throttle:5,60'])

  // Offer/Products - Produtos Agenda Ofertas
  Route.get('offers/products/progress', 'Offer/OfferProductController.productsOffersProgress').middleware(['throttle:30,60'])
  Route.get('offers/products/verify', 'Offer/OfferProductController.verifyProductsInOffer').middleware(['throttle:30,60'])

  Route.get('offers/:idOffer/products', 'Offer/OfferProductController.index').middleware(['throttle:30,60', 'check:OFFERS,VIEW_ONE'])
  Route.post('offers/:idOffer/products', 'Offer/OfferProductController.store').middleware(['throttle:5,60', 'check:OFFERS,EDIT'])
  //Route.get('offers/products/:idOfferProduct', 'Offer/OfferProductController.show').middleware(['throttle:30,60', 'check:OFFERS,VIEW_ONE'])
  Route.patch('offers/products/:idOfferProduct', 'Offer/OfferProductController.update').middleware(['throttle:5,60', 'check:OFFERS,EDIT'])
  //Route.delete('offers/products/:idOfferProduct', 'Offer/OfferProductController.delete').middleware(['throttle:5,60', 'check:OFFERS,EDIT'])
  //Route.delete('offers/:idOffer/products/clear', 'Offer/OfferProductController.clear').middleware(['throttle:5,60', 'check:OFFERS,EDIT'])

  Route.get('offers/:idOffer/products/pricegroup/:idOfferProduct?', 'Offer/OfferProductGroupPriceController.index').middleware(['throttle:30,60'])
  
  // Listas de compras
  Route.get('shopping/lists', 'ShoppingList/ShoppingListController.index').middleware(['throttle:30,60'])
  Route.get('shopping/lists/:idList', 'ShoppingList/ShoppingListController.show').middleware(['throttle:30,60'])
}).prefix('v1').middleware(['auth:jwt_email'])

Route.group(() => {
  // Produtos de oferta em andamento
  Route.get('offers/progress/products', 'Offer/Public/OfferProductPublicController.productsOffersProgress').middleware(['throttle:30,60'])

  // Busca as seções apartir de um tipo de seção
  Route.get('section/types/:idType', 'Product/Section/Public/SectionTypePublicController.show').middleware(['throttle:30,60'])

  // Produtos
  Route.get('products', 'Product/Public/ProductController.index').middleware(['throttle:30,60'])
  Route.get('products/:idProduct', 'Product/Public/ProductController.show').middleware(['throttle:30,60'])
  Route.get('products/section/:idSection', 'Product/Public/ProductController.indexProductsSection').middleware(['throttle:30,60'])
  
  // Shopping Lists
  Route.get('shopping/lists/:idList/copy', 'Public/ShoppingLists/ShoppingListsController.copyListNewUser').middleware(['throttle:30,60', 'auth:jwt_email'])
  Route.get('shopping/lists/public', 'Public/ShoppingLists/ShoppingListsController.getAllPublic').middleware(['throttle:30,60'])
  
  Route.get('shopping/lists', 'Public/ShoppingLists/ShoppingListsController.index').middleware(['throttle:30,60', 'auth:jwt_email'])
  Route.get('shopping/lists/:idList', 'Public/ShoppingLists/ShoppingListsController.show').middleware(['throttle:30,60', 'auth:jwt_email'])
  Route.post('shopping/lists', 'Public/ShoppingLists/ShoppingListsController.store').middleware(['throttle:30,60', 'auth:jwt_email'])
  Route.patch('shopping/lists/:idList', 'Public/ShoppingLists/ShoppingListsController.update').middleware(['throttle:30,60', 'auth:jwt_email'])
  Route.delete('shopping/lists/:idList', 'Public/ShoppingLists/ShoppingListsController.delete').middleware(['throttle:30,60', 'auth:jwt_email'])
  
  Route.get('shopping/lists/:idList/product/public', 'Public/ShoppingLists/ShoppingListProductsController.getAllPublic').middleware(['throttle:30,60', 'auth:jwt_email'])
  
  Route.get('shopping/lists/:idList/product', 'Public/ShoppingLists/ShoppingListProductsController.index').middleware(['throttle:30,60', 'auth:jwt_email'])
  Route.post('shopping/lists/product', 'Public/ShoppingLists/ShoppingListProductsController.store').middleware(['throttle:30,60', 'auth:jwt_email'])
  Route.patch('shopping/lists/:idList/product/:idListProduct', 'Public/ShoppingLists/ShoppingListProductsController.update').middleware(['throttle:30,60', 'auth:jwt_email'])
  Route.delete('shopping/lists/:idList/product/:idListProduct', 'Public/ShoppingLists/ShoppingListProductsController.delete').middleware(['throttle:30,60', 'auth:jwt_email'])
}).prefix('v1/app')


Route.group(() => {
  // Lista de compra compartilhada
  Route.get('shopping/list/:idList', 'Public/ShoppingLists/ShoppingListsController.showListShared').middleware(['throttle:30,60'])
}).prefix('v1/web')
